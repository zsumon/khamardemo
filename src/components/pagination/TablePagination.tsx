import React from 'react';
import {Button} from "reactstrap";

interface TablePaginationProps {
  currentPage: number,
  totalPage: number,
  rowsPerPage: number,

  onNextPage(): void,

  onPrevPage(): void,

  onRowPerPageChange(perPage: number): void,
}

const TablePagination = (props: TablePaginationProps) => {

  return <div className={"d-flex"}>
    <div className="d-flex mr-1">
      <span>Rows per page&nbsp;</span>
      <select onChange={(e) => {
        props.onRowPerPageChange(parseInt(e.target.value))
      }}>
        <option>5</option>
        <option>10</option>
        <option>15</option>
        <option>30</option>
      </select>
    </div>

    <div>
      <Button className={"mr-1"}
              disabled={props.currentPage === 0}
              onClick={() => props.onPrevPage()}>
        prev
      </Button>

      {(props.currentPage * props.rowsPerPage + 1) + " to " + Math.min((props.currentPage * props.rowsPerPage + props.rowsPerPage), props.totalPage) + " of " + props.totalPage}

      <Button
        className={"ml-1"}
        disabled={(props.currentPage * props.rowsPerPage + props.rowsPerPage) >= props.totalPage}
        onClick={() => props.onNextPage()}
      >next
      </Button>
    </div>
  </div>;
}

export default TablePagination;