import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem} from 'reactstrap';

import {AppAsideToggler, AppNavbarBrand, AppSidebarToggler} from '@coreui/react';
import logo from '../../assets/img/logo.svg'
import sygnet from '../../assets/img/sygnet.svg'
import {FormattedMessage} from "react-intl";


const defaultProps = {};

class DefaultHeader extends Component {

  handleLogout = (e) => {
    localStorage.removeItem("jwt");
    window.location.reload();
  };

  render() {

    // eslint-disable-next-line
    const {children, ...attributes} = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile/>
        <AppNavbarBrand
          full={{src: logo, width: 89, height: 25, alt: 'CoreUI Logo'}}
          minimized={{src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo'}}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg"/>

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link"><FormattedMessage
              id={"dashboard-title"}/></NavLink>
          </NavItem>
          <NavItem className="px-3 d-none">
            <NavLink to="#" className="nav-link"><FormattedMessage id={"settings-title"}/></NavLink>
          </NavItem>
          <NavItem className="px-3">
            <button onClick={() => this.props.onChangeButtonClick()}
                    className="nav-link btn btn-outline-info">
              <FormattedMessage id={"change-language"}/>
            </button>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link">
              <i className="icon-bell"/><Badge pill color="danger">5</Badge></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-list"/></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-location-pin"/></NavLink>
          </NavItem>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              {/*<img src={'../../assets/img/avatars/6.jpg'} className="img-avatar"
                   alt="admin@ri_dev.com"/>*/} <i style={{fontSize: "2rem"}} className="fa fa-user-circle"/>
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem header tag="div"
                            className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge
                color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge
                color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge
                color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge
                color="warning">42</Badge></DropdownItem>
              <DropdownItem header tag="div"
                            className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge
                color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge
                color="primary">42</Badge></DropdownItem>
              <DropdownItem divider/>
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
              <DropdownItem onClick={(e) => this.handleLogout(e)}><i
                className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none"/>
        {/*{<AppAsideToggler className="d-lg-none" mobile/>*/}
      </React.Fragment>
    );
  }
}

export default DefaultHeader;
