import React from "react";
import Async from "react-select/async";
import {searchCow} from "../../services/HttpService";

interface SearchCowProps {
  onCowSelected(value: string): void,
}

export default function SearchCow(props: SearchCowProps) {

  const loadOption = async (input: string): Promise<{ value: string, label: string } | null> => {
    //implement searching query in backend
    if (input.length < 2) return null;
    const cows = await searchCow(input);
    return cows.data.map((it: any) => ({value: it, label: it}));
  }

  return (
    <div>
      <Async
        noOptionsMessage={() => null}
        placeholder={"Type cow name to search"}
        components={{DropdownIndicator: () => null, IndicatorSeparator: () => null}}
        onChange={(selectedOption: any) => {
          props.onCowSelected(selectedOption.value)
        }}
        loadOptions={loadOption}/>
    </div>
  );
}