import React, {useState} from "react";
import {
  Card,
  CardBody,
  CardHeader, Col, Label, Row
} from 'reactstrap';
import SearchCow from "../../search_cow/SearchCow";
import EditReproduction from "../cattle_profile/edit_profile/EditReproduction";
import {logout, isValidToken} from "../../../services/HttpService";

export default function Reproduction(props: any) {
  const [selectedCow, setSelectedCow] = useState('');

  if (!isValidToken(localStorage.jwt)) return logout();

  return <div className={"mt-2"}>
    <Row>
      <Col md="8" lg="6">
        <Card className={""}>
          <CardHeader><strong>Add reproduction</strong></CardHeader>
          <CardBody>
            <Label>Select cow then input conceive info</Label>
            <SearchCow onCowSelected={(cowName) => setSelectedCow(cowName)}/>

            <EditReproduction disabled={selectedCow === ""} name={selectedCow} onSave={() => setSelectedCow('')}/>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </div>
}