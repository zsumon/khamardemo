import React from "react";
import Select from "react-select";
import Creatable, {makeCreatableSelect} from "react-select/creatable";

interface RenderMultiSelectProps {
  creatable?: boolean,
  isMulti?: boolean,
  options: string[],
  className?: string,
  style?: any,

  onChange(selectedOption: any): void,

  placeholder: string
}

export default function RenderMultiSelect(props: RenderMultiSelectProps) {

  return (
    <>
      {props.creatable ?
        <Creatable
          isMulti={props.isMulti}
          name="colors"
          options={props.options.map(x => makeOption(x))}
          className={"basic-multi-select " + props.className}
          classNamePrefix="select"
          closeMenuOnSelect={false}
          onChange={props.onChange}
          placeholder={props.placeholder}
        /> : <Select
          isMulti={props.isMulti}
          name="colors"
          options={props.options.map(x => makeOption(x))}
          className={"basic-multi-select " + props.className}
          classNamePrefix="select"
          closeMenuOnSelect={false}
          onChange={props.onChange}
          placeholder={props.placeholder}
        />}
    </>
  );

  function makeOption(x: string) {
    return {value: x, label: x};
  }
}