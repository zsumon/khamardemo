import React, {useState} from "react";
import {
  Button,
  FormGroup,
  Input
} from "reactstrap";

import cattleAvatar from '../../../assets/img/cattle_img.jpg';

interface InputRowProps {
  placeholder1: string,
  placeholder2: string,
  noBorder?: string,
  cattleName: string,

  onSaveClick(firstInput: string, secondInput: string): void

}

export default function InputRow(props: InputRowProps) {
  const [date, setDate] = useState(new Date().toJSON().slice(0, 10).split('-').reverse().toString().replace(/,/g, '-'));

  const [firstInput, setFirstInput] = useState('');
  const [secondInput, setSecondInput] = useState(props.placeholder2);

  return (
    <FormGroup
      className={`d-flex align-items-center justify-content-start pb-2 ${!props.noBorder && " border-bottom"}`}>

      <div className={"d-flex align-items-center"}>
        <img
          className={"mr-2"}
          style={{
            height: "3rem", width: "3rem"
          }} src={cattleAvatar} alt="cattle img"/>

        <div className={"d-sm-flex flex-sm-column d-md-flex flex-md-row align-items-md-baseline"}>
          <p className={"mr-2"} style={{minWidth: "4rem"}}>{props.cattleName}</p>
          <div className={"d-flex"}>
            <Input
              onChange={(e) => setFirstInput(e.target.value)}
              type={"text"}
              style={{maxWidth: "15rem"}}
              className={"mr-2"}
              value={firstInput}
              placeholder={props.placeholder1}/>
            <Input
              onChange={(e) => setSecondInput(e.target.value)}
              className={"mr-2"}
              type={"text"}
              value={secondInput}
              style={{maxWidth: "10rem"}}
              placeholder={props.placeholder2}/>

            <Button
              onClick={() => {
                props.onSaveClick(firstInput, secondInput);
                setFirstInput('');
                setSecondInput(props.placeholder2);
              }}
              color={"primary"}>Save</Button>
          </div>
        </div>
      </div>
    </FormGroup>
  );
}