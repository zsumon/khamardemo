import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Label,
  Pagination,
  PaginationItem, PaginationLink,
  Table
} from "reactstrap";

export function InputAddon(props) {

  return (
    <FormGroup>
      <div className={props.className}>
        <FormGroup className={"float-left mr-2"}>
          <Input type={"text"}

                 placeholder={props.placeholder}
                 onChange={(e) => {

                 }}/>
        </FormGroup>
        <Button className={"float-left"} onClick={(e) => {
          e.preventDefault();

        }}>Add</Button>
      </div>
    </FormGroup>
  );
}