import React from "react";
import {
  Table,
  Card,
  CardBody,
  CardHeader,
  Pagination,
  PaginationItem, PaginationLink
} from "reactstrap";

export default function DairyFeed(props) {
  return (
    <div className={"container mt-2"}>

      <Card>
        <CardHeader><strong>Daily feed</strong></CardHeader>

        <CardBody>
          <Table responsive>
            <thead>
            <tr>
              <th>Cattle</th>
              <th>Category</th>
              <th>Daily plan</th>
              <th>Done</th>
            </tr>
            </thead>

            <tbody>
            <tr>
              <td>cattle 1</td>
              <td>New born cattle</td>
              <td>Grass 1 time 2 KG <br/>
                Straw 2 times 4 KG each
              </td>
              <td>
                <div className="custom-checkbox custom-control">
                  <input type="checkbox"
                         id="done_id1"
                         className="custom-control-input"/>
                  <label
                    className="custom-control-label"
                    htmlFor="done_id1"/>
                </div>
              </td>
            </tr>

            </tbody>
          </Table>

          <Pagination>
            <PaginationItem>
              <PaginationLink previous tag="button"/>
            </PaginationItem>
            <PaginationItem active>
              <PaginationLink tag="button">1</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink tag="button">2</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink tag="button">3</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink tag="button">4</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink next tag="button"/>
            </PaginationItem>
          </Pagination>


        </CardBody>

      </Card>
    </div>
  );
}