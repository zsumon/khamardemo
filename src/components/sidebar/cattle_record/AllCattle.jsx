import * as React from "react";
import {
  Col,
  Row,
  Card,
  CardBody,
  CardHeader,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink, Button
} from "reactstrap";
import {Link, Redirect} from "react-router-dom";

import {FormattedMessage} from "react-intl"
import {Suspense, useEffect, useState} from "react";
import axios from "axios";
import {BASE_URL, clearJwt, isValidToken, paginatedGetCow} from "../../../services/HttpService";
import {useHistory} from "react-router";
import TablePagination from "../../pagination/TablePagination";

function AllCattle(props) {

  const [cattle, setCattle] = useState([]);
  const [validToken, setValidToken] = useState(true);

  /*
  * For pagination
  * */
  const [currPage, setCurrPage] = useState(0);
  const [perPage, setPerPage] = useState(5);
  const [paginatedCows, setPaginatedCows] = useState([]);

  useEffect(() => {
    if (!isValidToken(localStorage.jwt)) {
      setValidToken(false);
    }

    paginatedGetCow(currPage, perPage).then(res => {
      setPaginatedCows(res.data);
    }).catch(err => console.log(err));

    const bearerToken = "Bearer " + localStorage.jwt
    const config = {
      headers: {'Authorization': bearerToken}
    }
    axios.get(BASE_URL + "/all-cattle", config)
      .then(res => {
        console.log(res.data)
        setCattle(res.data.map(it => {
          const {name, tag, type, geneticType, geneticPercentage, dateOfBirth, image} = it;
          return {name, tag, type, geneticType, geneticPercentage, dateOfBirth, image};
        }))
      }).catch(er => console.log(er));
  }, []);

  const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>;

  if (!validToken) {
    clearJwt();
    return (
      <Redirect to={{
        pathname: "/login",
        state: {from: props.location}
      }}/>
    );
  }

  return (
    <Suspense fallback={loading()}>
      <div className={" mt-2"}>
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/> <FormattedMessage id="cattle-table.title"/>
                <Link to={"/register-cow"}>
                  <Button
                    className={"ml-2 align-right"} outline
                    color={"primary"}>
                    <i className="fa fa-plus mr-2"/> <FormattedMessage
                    id="cattle-table.add-btn"/>
                  </Button>
                </Link>
              </CardHeader>
              <CardBody>

                <Table responsive>
                  <thead>
                  <tr>
                    <th>Image</th>
                    <th><FormattedMessage id="name_id"/></th>
                    <th><FormattedMessage id="type"/></th>
                    <th><FormattedMessage id="genetic_type_and_percentage"/></th>
                    <th><FormattedMessage id="date_of_birth"/></th>
                  </tr>
                  </thead>

                  <tbody>
                  {paginatedCows.map((it, i) => (
                    <tr key={i}>
                      <td><img style={{maxWidth: "4rem", maxHeight: "4rem"}} src={it.image} alt=""/></td>
                      <td>{it.name}</td>
                      <td>{it.type}</td>
                      <td>{it.geneticType} {it.geneticPercentage}%</td>
                      <td>{it.dateOfBirth}</td>
                    </tr>
                  ))}
                  </tbody>
                </Table>

                <TablePagination
                  rowsPerPage={perPage}
                  totalPage={cattle.length}
                  currentPage={currPage}
                  onNextPage={() => {
                    paginatedGetCow(currPage + 1, perPage).then(res => {
                      setPaginatedCows(res.data);
                    }).catch(err => console.log(err));
                    setCurrPage(currPage => currPage + 1)
                  }}
                  onPrevPage={() => {
                    paginatedGetCow(currPage - 1, perPage).then(res => {
                      setPaginatedCows(res.data);
                    }).catch(err => console.log(err));
                    setCurrPage(currPage => currPage - 1)
                  }}
                  onRowPerPageChange={(rowsPerPage) => {
                    paginatedGetCow(0, rowsPerPage).then(res => {
                      setPaginatedCows(res.data);
                    }).catch(err => console.log(err));
                    setPerPage(rowsPerPage);
                    setCurrPage(0)
                  }}/>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </Suspense>
  );
}

export default AllCattle;
