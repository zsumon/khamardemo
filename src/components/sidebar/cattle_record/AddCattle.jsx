import * as React from "react";
import {Button, Card, CardBody, CardHeader, Form, FormGroup, Input, Label, CustomInput} from "reactstrap"
import {Link} from "react-router-dom";
import {useState} from "react";
import {useIntl} from 'react-intl';
import {DATE_PLACEHOLDER} from "../../../constants/ExtraConstants";
import {Row, Col} from "reactstrap";
import {logout, isValidToken, saveCow, saveWeight} from "../../../services/HttpService";
import {useHistory} from "react-router";
import loading16 from '../../../assets/img/loading16.gif';
import {storage} from "../../../firebase";

export default function AddCattle(props) {

  const history = useHistory();
  const [cattleType, setCattleType] = useState('Fattening');

  function handleCattleTypeChange(e) {
    e.preventDefault();
    setCattleType(e.target.value);
  }

  const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>;
  /*
  * Form field values
  * will be used to submit the data to the server
  * */
  const [name, setName] = useState('');
  const [geneticType, setGeneticType] = useState('Jersey');
  const [geneticPercentage, setGeneticPercentage] = useState('');
  const [dateOfBirth, setDteOfBirth] = useState(new Date().toJSON().slice(0, 10));
  //const [date, setDate] = useState(new Date().toJSON().slice(0, 10));
  const [weight, setWeight] = useState('');

  const [currentPregnancyStatus, setCurrentPregnancyStatus] = useState(false);
  const [dateOfPregnancy, setDateOfPregnancy] = useState(new Date().toJSON().slice(0, 10));
  const [lastDateOfGivingBirth, setLastDateOfGivingBirth] = useState(new Date().toJSON().slice(0, 10));
  const [age, setAge] = useState('');

  const [imageInBase64, setImageInBase64] = useState('');
  const [imageAsFile, setImageAsFile] = useState();
  const [uploadingImage, setUploadingImage] = useState(false);

  function handleSubmit() {
    const cattleToAdd = {
      "name": name,
      "type": cattleType,
      "geneticType": geneticType,
      "geneticPercentage": geneticPercentage,
      "dateOfBirth": dateOfBirth,
      "currentPregnancyStatus": currentPregnancyStatus,
      "dateOfPregnancy": dateOfPregnancy,
      "lastDateOfGivingBirth": lastDateOfGivingBirth,
      "image": "",
    }
    if (age) {
      cattleToAdd.age = age;
      cattleToAdd.ageTakingDate = (new Date()).toJSON().slice(0, 10);
    } else {
      alert("Enter age");
      return;
    }

    if (name.trim() === "") {
      alert("invalid cow name");
      return;
    }
    const gp = parseInt(geneticPercentage.trim());
    if (gp <= 0 || gp > 100 || geneticPercentage.trim() === "" || isNaN(gp)) {
      alert("invalid genetic percentage");
      return;
    }
    if (dateOfBirth.trim() === "") {
      alert("invalid date Of birth");
      return;
    }
    const w = parseInt(weight);
    if (w <= 0 || w > 1000 || weight.trim() === "" || isNaN(w)) {
      alert("invalid weight");
      return;
    }

    function saveData() {
      saveCow(cattleToAdd).then(res => {
        // console.log('added cow:', res.data);
        if (res.data === "cow already exists") {
          alert("cow already exists");
        } else {
          const dt = new Date().toJSON().slice(0, 10);
          saveWeight(name, {weight: parseInt(weight), dateYYYYMMDD: dt}).then(res => {
            // console.log('saved weight after adding cow', res.data);
            history.push("/all-cow");
          }).catch(err => console.log('error saving weight after adding cow', err));
        }
      }).catch(err => console.log('error adding cow: ', err));
    }

    if (imageAsFile) {
      setUploadingImage(true);
      const uploadTask = storage.ref(`/khamar360Images/${name}_${imageAsFile.name}`).put(imageAsFile);
      uploadTask.on('state_changed', (snapShot) => console.log(snapShot),
        (err) => console.log(err),
        () => {
          // gets the functions from storage references the image storage in firebase by the children
          // gets the download url then sets the image from firebase as the value for the imgUrl key:
          storage.ref('khamar360Images/' + name + "_" + imageAsFile.name).getDownloadURL()
            .then(fireBaseUrl => {
              setUploadingImage(false);
              cattleToAdd.image = fireBaseUrl;
              saveData();
            })
        })
    } else {
      saveData();
    }
  }

  const intl = useIntl();

  if (!isValidToken(localStorage.jwt)) return logout();

  return (
    <Row className={"mt-2"}>
      <Col md={8} lg={6}>
        <Form className={"pb-5"}>
          <Card>
            <CardHeader>
              <strong>Cattle related info</strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md={8} lg={6}>
                  <FormGroup>
                    <Label for="cattle_name">{intl.formatMessage({id: 'cattle_name'})}</Label>
                    <Input
                      type="text"
                      id="cattle_name"
                      placeholder={intl.formatMessage({id: 'enter.cattle_name'})}
                      onChange={(e) => {
                        setName(e.target.value)
                      }}
                      required/>

                  </FormGroup>

                  <FormGroup>
                    <Label for="cattle_type">{intl.formatMessage({id: 'cattle_type'})}</Label>
                    <Input type={"select"} onChange={handleCattleTypeChange}>
                      <option>{intl.formatMessage({id: 'cattle_type.fattening'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_type.dairy'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_type.calf'})}</option>
                    </Input>
                  </FormGroup>


                  <FormGroup>
                    <Label for="cattle_genetic_type">{intl.formatMessage({id: 'cattle_genetic_type'})}</Label>
                    <Input
                      style={{maxWidth: "10rem"}}
                      type={"select"}
                      onChange={(e) => {
                        setGeneticType(e.target.value)
                      }}>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option1'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option2'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option3'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option4'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option5'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option6'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option7'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option8'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option9'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option10'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option11'})}</option>
                      <option>{intl.formatMessage({id: 'cattle_genetic_type.option12'})}</option>

                    </Input>
                  </FormGroup>


                  <FormGroup>
                    <Label
                      for="cattle_genetic_percentage">{intl.formatMessage({id: 'cattle_genetic_percentage'})}</Label>
                    <div className="d-flex align-items-baseline">
                      <Input
                        className={"mr-2"}
                        style={{maxWidth: "8rem"}}
                        type="text"
                        id="cattle_genetic_percentage"
                        onChange={(e) => {
                          setGeneticPercentage(e.target.value)
                        }}
                        required/> <p>%</p>
                    </div>
                  </FormGroup>

                  <FormGroup>
                    <Label for="cattle_age">Age</Label>
                    <Input
                      onChange={(e) => setAge(Number(e.target.value))}
                      style={{maxWidth: "8rem"}} type={"text"} placeholder={"Enter age"}/>
                  </FormGroup>

                  <FormGroup className={"d-none"}>
                    <Label for="cattle_birth_date">{intl.formatMessage({id: 'cattle_birth_date'})}</Label>
                    <Input
                      disabled={age}
                      style={{maxWidth: "11rem"}}
                      type="date"
                      value={dateOfBirth}
                      onChange={(e) => {
                        setDteOfBirth(e.target.value)
                      }}
                      placeholder={DATE_PLACEHOLDER}
                      id="cattle_birth_date"
                      required/>
                  </FormGroup>

                  <FormGroup>
                    <Label for="cattle_weight">{intl.formatMessage({id: 'cattle_weight'})}</Label>

                    <Input
                      style={{maxWidth: "7rem"}}
                      type="text"
                      id="cattle_weight"
                      onChange={(e) => {
                        setWeight(e.target.value)
                      }}
                      required/>
                  </FormGroup>

                  {cattleType === "Dairy" &&

                  <FormGroup>
                    <Label for="">Currently pregnant?</Label>
                    <div>
                      <CustomInput
                        checked={currentPregnancyStatus}
                        onChange={(e) => {
                          e.target.checked ? setCurrentPregnancyStatus(true) : setCurrentPregnancyStatus(false);
                        }}
                        type="radio"
                        id="cps_yes"
                        name="pregnancy_status"
                        label="Yes"/>
                      <CustomInput
                        onChange={(e) => {
                          e.target.checked ? setCurrentPregnancyStatus(false) : setCurrentPregnancyStatus(true);
                        }}
                        type="radio"
                        id="cps_no"
                        name="pregnancy_status"
                        label="No"/>
                    </div>

                    {currentPregnancyStatus ?
                      <FormGroup>
                        <Label>Date of pregnancy</Label>
                        <Input
                          value={dateOfPregnancy}
                          onChange={(e) => setDateOfPregnancy(e.target.value)}
                          style={{maxWidth: "11rem"}} type={"date"}/>
                      </FormGroup> :
                      <FormGroup>
                        <Label>Last date of giving birth</Label>
                        <Input
                          value={lastDateOfGivingBirth}
                          onChange={(e) => setLastDateOfGivingBirth(e.target.value)}
                          type={"date"} style={{maxWidth: "11rem"}}/>
                      </FormGroup>}

                  </FormGroup>}

                  {(cattleType === "Calf") &&
                  <div>
                    <FormGroup>
                      <Label>Mother</Label>
                      <Input style={{maxWidth: "10rem"}} type={"text"}/>
                    </FormGroup>
                  </div>}

                  {imageInBase64 && <img src={imageInBase64} style={{height: "8rem", width: "8rem"}} alt=""/>}

                  <FormGroup>
                    <Label>Image</Label>
                    <Input
                      type={"file"} accept="image/*" onChange={(e) => {
                      const file = e.target.files[0];
                      setImageAsFile(file);
                      const reader = new FileReader();
                      if (file) {
                        reader.readAsDataURL(file);
                      }
                      reader.onload = () => {
                        setImageInBase64(reader.result.toString());
                      }
                    }}/>

                  </FormGroup>

                </Col>
              </Row>

            </CardBody>
          </Card>

          <Button
            onClick={handleSubmit}
            className={"mr-2"} color="primary">{intl.formatMessage({id: 'text.submit'})}</Button>
          <Link to={"/all-cow"} className={"btn "}
                style={{backgroundColor: "red", color: "white"}}>{intl.formatMessage({id: 'text.cancel'})}</Link>

          {uploadingImage && <> <span>{'  '}</span> <img src={loading16} alt=""/></>}
        </Form>
      </Col>
    </Row>
  );
}
