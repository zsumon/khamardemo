import React, {useState} from "react";
import {Form, Label, Button, CardBody, Card, CardHeader, FormGroup, Input} from "reactstrap";
import {FormattedMessage} from "react-intl";
import FeedIngredientList from "./FeedIngredientList";
import MakeFeedMixture from "./feed-distribution/MakeFeedMixture";
import TotalMixRation from "./feed-distribution/TotalMixRation";

export default function AddFeedIngredient(props) {
  return (
    <>
      <div className={"mt-2"}>
        <Card>
          <CardHeader><strong>Add Feed Ingredient</strong></CardHeader>
          <CardBody>

            <FormGroup>
              <Label for="feed_ing_type"><FormattedMessage id="feed_ingredient.type"/></Label>
              <Input type={"select"}
                     id={"feed_ing_type"}>

                <FormattedMessage id={"feed_ingredient.type.option1"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_ingredient.type.option2"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_ingredient.type.option3"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>

              </Input>
            </FormGroup>

            <FormGroup>
              <Label for="feed_ing_name"><FormattedMessage id="feed_ingredient.name"/></Label>
              <Input type={"text"}
                     id={"feed_ing_name"}/>
            </FormGroup>

            <FormGroup>
              <Label for="feed_ing_type"><FormattedMessage id="feed_ingredient.nutrition"/></Label>
              <Input type={"select"}
                     id={"feed_ing_type"}>

                <FormattedMessage id={"feed_ingredient.nutrition.option1"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_ingredient.nutrition.option2"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_ingredient.nutrition.option3"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
              </Input>
            </FormGroup>

            <Button><FormattedMessage id={"text.save"}/></Button>

          </CardBody>
        </Card>
      </div>
      <FeedIngredientList/>

      <MakeFeedMixture/>

      <TotalMixRation/>
    </>
  );
}