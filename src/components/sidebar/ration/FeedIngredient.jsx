import React from "react";
import {CardBody, FormGroup, Input, Label} from "reactstrap";

export default function FeedIngredient(props) {
  return (
    <div>
      <FormGroup>
        <Label for={""}>খাবারের উপকরণ</Label>

        <FormGroup className={"ml-3"}>
          <Label className={"float-left mr-4 pr-2"}>দানাদারঃ</Label>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   id={""}
                   checked
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>ফিড</label>
          </div>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   id={""}
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>ভুট্টা</label>
          </div>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   id={""}
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>গমের ভুষি</label>
          </div>
        </FormGroup>
        <div className="clearfix"/>
        <FormGroup className={"ml-3"}>
          <Label className={"float-left mr-3"}>রাসায়নিকঃ</Label>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   checked
                   id={""}
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>ক্যালসিয়াম</label>
          </div>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   id={""}
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>জিঙ্ক</label>
          </div>
        </FormGroup>
        <div className="clearfix"/>
        <FormGroup className={"ml-3"}>
          <Label className={"float-left mr-5"}>আঁশঃ</Label>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   id={""}
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>খড়</label>
          </div>

          <div className="custom-checkbox custom-control float-left mr-2">
            <input type="checkbox"
                   id={""}
                   checked
                   className="custom-control-input"/>
            <label
              className="custom-control-label"
              htmlFor={""}>ভুষি</label>
          </div>

        </FormGroup>

      </FormGroup>
      <div className="clearfix"/>

      <FormGroup>
        <Label for={""}>পরিমাণ</Label>
        <Input placeholder={"পরিমান প্রদান করুন"}></Input>
      </FormGroup>
    </div>
  );
}