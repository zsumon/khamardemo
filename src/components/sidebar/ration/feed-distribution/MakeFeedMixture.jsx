import React, {useState} from "react";
import {
  Form,
  Label,
  Button,
  CardBody,
  Card,
  CardHeader,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon, InputGroupText, Table, Pagination, PaginationItem, PaginationLink
} from "reactstrap";
import {FormattedMessage} from "react-intl";
import {InputAddon} from "../../Addons/InputAddon";

export default function MakeFeedMixture(props) {
  return (
    <>
      <div className={"mt-2"}>
        <Card>
          <CardHeader><strong>Make Feed Mixture</strong></CardHeader>
          <CardBody>

            <FormGroup>
              <Label for="feed_ing_type"><FormattedMessage id="feed_ingredient.type"/></Label>
              <Input type={"select"}
                     id={"feed_ing_type"}>

                <FormattedMessage id={"feed_ingredient.type.option1"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_ingredient.type.option2"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_ingredient.type.option3"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>

              </Input>
            </FormGroup>

            <FormGroup>
              <Label for="feed_mix_name"><FormattedMessage id="feed_mixture.name"/></Label>
              <Input type={"text"}
                     placeholder={"Enter mixture name"}
                     id={"feed_mix_name"}/>
            </FormGroup>

            <FormGroup>
              <Label for="feed_mix_amount"><FormattedMessage id="text.amount"/></Label>
              <InputGroup>
                <Input placeholder="Amount"/>
                <InputGroupAddon addonType="append">
                  <InputGroupText>KG</InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>


            <div className={"mt-5"}>
              <strong><p>Mixed feed amount</p></strong>
              <Table responsive>
                <thead>
                <tr>
                  <th>Ingredient</th>
                  <th>Nutrition</th>
                  <th>Amount (KG)</th>
                  <th/>
                </tr>
                </thead>

                <tbody>
                <tr>
                  <td>ধানের কুড়া</td>
                  <td>শর্করা</td>
                  <td><Input type={"text"} value={"40"}/></td>
                  <td><Button><i className="fa fa-trash"/></Button></td>
                </tr>
                <tr>
                  <td>খড়</td>
                  <td>আমিষ</td>
                  <td><Input type={"text"} value={"40"}/></td>
                  <td><Button><i className="fa fa-trash"/></Button></td>
                </tr>
                <tr>
                  <td>ভুষি</td>
                  <td>আমিষ</td>
                  <td><Input type={"text"} value={"40"}/></td>
                  <td><Button><i className="fa fa-trash"/></Button></td>
                </tr>
                <tr>
                  <td>
                    <Input type={"select"}>
                      <option>গমের ভুষি</option>
                    </Input>
                  </td>
                  <td>
                    <Input type={"select"}>
                      <option>শর্করা</option>
                    </Input>
                  </td>
                  <td><Input type={"text"}/></td>
                  <td>
                    <Button><i className="fa fa-plus"/></Button>
                  </td>

                </tr>
                </tbody>
              </Table>

              <Button><FormattedMessage id={"text.save"}/></Button>
            </div>

          </CardBody>
        </Card>
      </div>
    </>
  );
}