import React, {useState} from "react";
import {Form, Label, Button, CardBody, Card, CardHeader, FormGroup, Input, Table} from "reactstrap";
import {FormattedMessage} from "react-intl";

export default function TotalMixRation(props) {
  return (
    <div>
      <Card>
        <CardHeader><strong>Total mix ration</strong></CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="feed_dist.cattle_type"><FormattedMessage id="feed_dist.cattle_type"/></Label>
              <Input type={"select"}
                     id={"feed_dist.cattle_type"}>

                <FormattedMessage id={"feed_dist.cattle_type.option1"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_dist.cattle_type.option2"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>
                <FormattedMessage id={"feed_dist.cattle_type.option3"}>
                  {opt => <option>{opt}</option>}
                </FormattedMessage>

              </Input>
            </FormGroup>
            <FormGroup>

              <Label for="feed_dist.ration_name"><FormattedMessage id="feed_dist.ration_name"/></Label>
              <Input type={"text"}
                     value={"দানাদার মিশ্রণ ০৬-০৫-২০২০"}
                     id={"feed_dist.cattle_type"}>
              </Input>
            </FormGroup>


            <Table responsive>
              <thead>
              <tr>
                <th>Ingredient</th>
                <th>Amount (KG)</th>
                <th>Rate</th>
                <th/>
              </tr>
              </thead>

              <tbody>
              <tr>
                <td>দানাদার মিশ্রণ ০৬-০৫-২০২০</td>
                <td><Input type={"text"} value={"40"}/></td>
                <td><Input type={"text"} value={"40"}/></td>
                <td><Button><i className="fa fa-trash"/></Button></td>
              </tr>
              <tr>
                <td>ACI Dairy Feed</td>
                <td><Input type={"text"} value={"40"}/></td>
                <td><Input type={"text"} value={"40"}/></td>
                <td><Button><i className="fa fa-trash"/></Button></td>
              </tr>
              <tr>
                <td>ACI DCP Gold</td>
                <td><Input type={"text"} value={".15"}/></td>
                <td><Input type={"text"} value={".15"}/></td>
                <td><Button><i className="fa fa-trash"/></Button></td>
              </tr>
              <tr>
                <td>
                  <Input type={"select"}>
                    <option></option>
                  </Input>
                </td>
                <td>
                  <Input type={"text"}/>
                </td>
                <td><Input type={"text"}/></td>
                <td>
                  <Button><i className="fa fa-plus"/></Button>
                </td>

              </tr>
              </tbody>
            </Table>

            <Button><FormattedMessage id={"text.save"}/></Button>

          </Form>

        </CardBody>
      </Card>
    </div>
  );
}