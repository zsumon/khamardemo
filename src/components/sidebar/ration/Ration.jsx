import React, {useState} from "react";
import {Form, Label, Button, CardBody, Card, CardHeader, FormGroup, Input} from "reactstrap";
import FeedIngredient from "./FeedIngredient";

export default function Ration(props) {
  const [rationType, setRationType] = useState('দুগ্ধ/ব্রিডিং');
  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader><strong>Ration info</strong></CardHeader>
        <CardBody>
          <FormGroup>
            <Label>Ration name</Label>
            <Input type={"text"}/>
          </FormGroup>
          <FormGroup>
            <Label for={"ration_type"}>ধরণ</Label>
            <Input type={"select"} onChange={(e) => {
              setRationType(e.target.value);
            }}>
              <option>দুগ্ধ/ব্রিডিং</option>
              <option>মোটাতাজা</option>
            </Input>
          </FormGroup>

          {rationType === "দুগ্ধ/ব্রিডিং" && <>
            <FormGroup>
              <Label for={"pregnancy_duration"}>গৰ্ভলকালীন সময়</Label>
              <Input id={"pregnancy_duration"} type={"text"}/>
            </FormGroup>

            <FormGroup>
              <Label for={""}>বাচ্চার বয়স</Label>
              <Input/>
            </FormGroup>

            <FormGroup>
              <Label for={""}>গভীর ওজন</Label>
              <Input/>
            </FormGroup>
          </>
          }
          {rationType === "মোটাতাজা" && <>
            <FormGroup>
              <Label for={""}>বয়স</Label>
              <Input/>
            </FormGroup>

            <FormGroup>
              <Label for={""}>ওজন</Label>
              <Input/>
            </FormGroup>
          </>
          }

          <FeedIngredient/>

          {/* <FormGroup>
            <Label for={""}>খাবারের উপকরণ</Label>

            <FormGroup className={"ml-3"}>
              <Label className={"float-left mr-4 pr-2"}>দানাদারঃ</Label>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>
            </FormGroup>
            <div className="clearfix"/>
            <FormGroup className={"ml-3"}>
              <Label className={"float-left mr-3"}>রাসায়নিকঃ</Label>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>
            </FormGroup>
            <div className="clearfix"/>
            <FormGroup className={"ml-3"}>
              <Label className={"float-left mr-5"}>আঁশঃ</Label>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>

              <div className="custom-checkbox custom-control float-left mr-2">
                <input type="checkbox"
                       id={""}
                       className="custom-control-input"/>
                <label
                  className="custom-control-label"
                  htmlFor={""}>Feed</label>
              </div>
            </FormGroup>

          </FormGroup>
          <div className="clearfix"/>

          <FormGroup>
            <Label for={""}>পরিমাণ</Label>
            <Input></Input>
          </FormGroup>
*/}
          <Button>Add ration</Button>

        </CardBody>
      </Card>

    </div>
  );
}