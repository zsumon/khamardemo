import React, {useState} from "react";
import {
  Form,
  Label,
  Button,
  CardBody,
  Card,
  CardHeader,
  FormGroup,
  Input,
  Table,
  Pagination,
  PaginationItem, PaginationLink
} from "reactstrap";

export default function FeedIngredientList(props) {
  return (
    <div>
      <Card>
        <CardHeader><strong>Feed Ingredient List</strong></CardHeader>
        <CardBody>
          <Table responsive>
            <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Nutrition</th>
            </tr>
            </thead>

            <tbody>
            <tr>
              <td>গমের ভুষি</td>
              <td>দানাদার</td>
              <td>শর্করা</td>
            </tr>
            <tr>
              <td>ধানের কুড়া</td>
              <td>দানাদার</td>
              <td>শর্করা</td>
            </tr>
            <tr>
              <td>খড়</td>
              <td>আঁশ</td>
              <td>আমিষ</td>
            </tr>
            <tr>
              <td>ভুষি</td>
              <td>আঁশ</td>
              <td>আমিষ</td>
            </tr>

            </tbody>
          </Table>

          {/*<TablePagination>
            <PaginationItem>
              <PaginationLink previous tag="button"/>
            </PaginationItem>
            <PaginationItem active>
              <PaginationLink tag="button">1</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink tag="button">2</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink tag="button">3</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink tag="button">4</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink next tag="button"/>
            </PaginationItem>
          </TablePagination>*/}
        </CardBody>
      </Card>

    </div>
  );
}