import React, {useState} from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Label, ListGroup, ListGroupItem,
  Pagination,
  PaginationItem, PaginationLink,
  Table
} from "reactstrap";

import cattleAvatar from '../../../assets/img/cattle_img.jpg';
import axios from 'axios';

export function WeightRow(props) {
  const [isSaving, setIsSaving] = useState(false);

  const [weight, setWeight] = useState(0);
  const [date, setDate] = useState(new Date().toJSON().slice(0, 10).split('-').reverse().toString().replace(/,/g, '-'));

  async function handleSave() {
    setIsSaving(true);
    if (!props.tag) return;
    let url = 'http://localhost:8080/add-weight/' + props.tag;
    let options = {
      method: 'POST',
      url: url,
      headers: {
        'Authorization': 'Bearer ' + localStorage.jwt,
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
      },
      data: {
        weight,
        weightTakingDate: date,
      }
    };

    try {
      let response = await axios(options);
      console.log(response);
    } catch (e) {
      console.log(e)
    }

    setTimeout(() => {
      setIsSaving(false);
      props.onWeightSaved(props.tag);
      setWeight(0)
    }, 1000)


  }

  return (
    <FormGroup
      className={`d-flex align-items-center justify-content-start pb-2 ${!props.noBorder && " border-bottom"}`}>

      <div className={"d-flex align-items-center"}>
        <img
          className={"mr-2"}
          style={{
            height: "3rem", width: "3rem"
          }} src={cattleAvatar} alt="cattle img"/>

        <div className={"d-sm-flex flex-sm-column d-md-flex flex-md-row align-items-md-baseline"}>
          <p className={"mr-2"} style={{minWidth: "5rem"}}>{props.tag}</p>
          <div className={"d-flex"}>
            <Input
              onChange={(e) => setWeight(e.target.value)}
              type={"text"}
              style={{maxWidth: "20rem"}}
              className={"mr-2"}
              placeholder={""}/>
            <Input
              onChange={(e) => setDate(e.target.value)}
              className={"mr-2"}
              type={"text"}
              style={{maxWidth: "7rem"}}
              placeholder={date}/>

            {isSaving ? <Button disabled>Saving..</Button> :
              <Button color={"primary"} onClick={handleSave}>Save</Button>}
          </div>
        </div>
      </div>
    </FormGroup>
  );
}