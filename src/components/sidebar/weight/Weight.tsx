import React, {useState} from "react";
import {
  Card,
  CardBody,
  CardHeader, Col, Label, Row
} from 'reactstrap';
import SearchCow from "../../search_cow/SearchCow";
import EditWeight from "../cattle_profile/edit_profile/EditWeight";
import {logout, isValidToken} from "../../../services/HttpService";

export default function Weight(props: any) {
  const [selectedCow, setSelectedCow] = useState('');

  if (!isValidToken(localStorage.jwt)) return logout();

  return <div className={"mt-2"}>
    <Row>
      <Col md="8" lg="6">
        <Card className={""}>
          <CardHeader><strong>Weight input</strong></CardHeader>
          <CardBody>
            <Label>Select cow then input weight info</Label>
            <SearchCow onCowSelected={(cowName) => {
              setSelectedCow(cowName);
            }}/>

            <EditWeight disabled={selectedCow === ""} name={selectedCow} onSave={() => setSelectedCow('')}/>

          </CardBody>
        </Card>
      </Col>
    </Row>
    <Row>

    </Row>
  </div>
}