import React, {useState} from "react";
import {Card, CardBody, CardHeader, Col, FormGroup, Label, Row, Input, Button, CustomInput} from "reactstrap";

enum VaccineType {
  MONTHLY, AGE_LIMIT, PREGNANCY_TIME, ONE_IN_LIFE
}

export default function VaccineRecord(props: any) {

  const [vaccineType, setVaccineType] = useState<VaccineType>(VaccineType.MONTHLY);

  return <div>
    <Card>
      <CardHeader>Vaccine record</CardHeader>
      <CardBody>
        <Row>
          <Col md={4} lg={3}>
            <FormGroup>
              <Label>Vaccine name</Label>
              <Input type={"text"} placeholder={"Enter vaccine name"}/>
            </FormGroup>

            <FormGroup>
              <Label for="">Vaccine type</Label>
              <div>
                <CustomInput
                  className={"mb-2"}
                  type="radio" id="vaccine-type1" name="customRadio" label="Monthly"
                  checked={vaccineType === VaccineType.MONTHLY}
                  onChange={(e) => {
                    if (e.target.checked)
                      setVaccineType(VaccineType.MONTHLY);
                  }}>
                  {vaccineType === VaccineType.MONTHLY &&
                  <FormGroup>
                    <Label for="">Months</Label>
                    <CustomInput type="checkbox" name="monthGroup" id="month_January" label="January"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_February" label="February"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_March" label="March"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_April" label="April"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_May" label="May"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_June" label="June"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_July" label="July"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_August" label="August"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_September" label="September"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_October" label="October"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_November" label="November"/>
                    <CustomInput type="checkbox" name="monthGroup" id="month_December" label="December"/>
                    <CustomInput
                      type="checkbox" id="month" name="month" label="Monthly"/>
                  </FormGroup>}
                </CustomInput>
                <CustomInput
                  className={"mb-2"}
                  onChange={(e) => {
                    if (e.target.checked)
                      setVaccineType(VaccineType.AGE_LIMIT);

                  }}
                  type="radio" id="vaccine-type2" name="customRadio" label="Age limit">
                  {vaccineType === VaccineType.AGE_LIMIT &&
                  <div className={"d-flex mb-2 mt-1"}>
                    <Input className={"mr-2"} type={"text"} placeholder={"From"}/> <Input type={"text"}
                                                                                          placeholder={"To"}/>
                  </div>}
                </CustomInput>

                <CustomInput
                  className={"mb-2"}
                  onChange={(e) => {
                    if (e.target.checked)
                      setVaccineType(VaccineType.PREGNANCY_TIME);

                  }}
                  type="radio" id="vaccine-type3" name="customRadio" label="Pregnancy time">
                  {vaccineType === VaccineType.PREGNANCY_TIME &&
                  <div className={"d-flex mb-2 mt-1"}>
                    <Input className={"mr-2"} type={"text"} placeholder={"From"}/> <Input type={"text"}
                                                                                          placeholder={"To"}/>
                  </div>}
                </CustomInput>

                <CustomInput
                  className={"mb-2"}
                  onChange={(e) => {
                    if (e.target.checked)
                      setVaccineType(VaccineType.ONE_IN_LIFE);
                  }} type="radio" id="vaccine-type4" name="customRadio" label="One in life"/>
              </div>


            </FormGroup>
          </Col>
        </Row>
      </CardBody>
    </Card>
  </div>

}