import React, {useState} from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardGroup,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Label, Table, Pagination, PaginationItem, PaginationLink, CustomInput, Tooltip
} from 'reactstrap';
import RenderMultiSelect from "../Addons/RenderMultiSelect.tsx";
import {FormattedMessage} from "react-intl";
import VaccineRecord from "./VaccineRecord";

export default function Vaccination(props) {
  let INF = Number(9999999999);
  const [diseaseName, setDiseaseName] = useState('');
  /* 1 to INF -> above
  * -INF to 10 -> below
  * 1 to 10 -> between*/
  const [ageRangeAtFirstDose, setAgeRangeAtFirstDose] = useState({
    start: 0,
    end: INF
  });
  const [boosterDoseRange, setBoosterDoseRange] = useState({
    start: 0,
    end: INF
  });
  const [subsequentDose, setSubsequentDose] = useState({
    onceInLifeTime: false,
    repeatEvery: 12 /*Month*/
  });
  const [route, setRoute] = useState('');
  const [availableVaccines, setAvailableVaccines] = useState([]);
  const [preferredMonth, setPreferredMonth] = useState([]);
  const [remarks, setRemarks] = useState('');
  const [vaccinationStatus, setVaccinationStatus] = useState(false);
  const [genderSpecific, setGenderSpecific] = useState('');

  const [firstDoseAgeRangeType, setFirstDoseAgeRangeType] = useState('Above');
  const [boosterDoseAgeRangeType, setBoosterDoseAgeRangeType] = useState('After');
  const [subsequentDoseAgeRangeType, setSubsequentDoseAgeRangeType] = useState('Repeat every');

  return <div className={"mt-2"}>
    <VaccineRecord/>
    <Card>
      <CardHeader><strong>Vaccination info</strong></CardHeader>

      <CardBody>
        <FormGroup>
          <Label for="">Name of the diseases</Label>
          <RenderMultiSelect
            creatable
            placeholder={"Select or type to create new"}
            options={['BOVILIS FMDV GEL, FUTVAC', 'BOVILIS HS', 'Black Quarter Vaccines']}
            onChange={(data) => {
              setDiseaseName(data.value);
            }}/>
        </FormGroup>

        <FormGroup>
          <Label for="">Age at first dose</Label>
          <FormGroup className={"d-flex"}>
            <Input
              type={"select"}
              onChange={(e) => {
                setFirstDoseAgeRangeType(e.target.value);
              }}>
              <option>Above</option>
              <option>Between</option>
              <option>Below</option>
            </Input>
            {(firstDoseAgeRangeType === "Above" || firstDoseAgeRangeType === "Below") && <div className={"ml-2"}>
              <Input
                onChange={(e) => {

                }}
                type={"text"}
                placeholder={"DD-MM-YY"}
              />
            </div>}

            {firstDoseAgeRangeType === "Between" && <div className={"ml-2 d-flex"}>
              <Input className={"mr-2"} type={"text"} placeholder={"DD-MM-YY"}/>
              <Input type={"text"} placeholder={"DD-MM-YY"}/>
            </div>}
          </FormGroup>
        </FormGroup>


        <FormGroup>
          <Label for="">Booster dose</Label>
          <FormGroup className={"d-flex"}>
            <Input
              type={"select"}
              onChange={(e) => {
                setBoosterDoseAgeRangeType(e.target.value);
              }}>
              <option>At</option>
              <option>After</option>
              <option>N/A</option>
            </Input>
            {
              (boosterDoseAgeRangeType === "After" || boosterDoseAgeRangeType === "At") && <div className={"ml-2"}>
                <Input
                  onChange={(e) => {

                  }}
                  type={"text"}
                  placeholder={"DD-MM-YY"}
                />
              </div>
            }
          </FormGroup>

        </FormGroup>

        <FormGroup>
          <Label for="">Subsequent dose</Label>

          <FormGroup className={"d-flex"}>
            <Input
              type={"select"}
              onChange={(e) => {
                setSubsequentDoseAgeRangeType(e.target.value);
              }}>
              <option>Repeat every</option>
              <option>Once in a lifetime</option>
            </Input>
            {
              (subsequentDoseAgeRangeType === "Repeat every") &&
              <div className={"ml-2"}>
                <Input
                  onChange={(e) => {

                  }}
                  type={"text"}
                  placeholder={"DD-MM-YY"}
                />
              </div>
            }
          </FormGroup>

        </FormGroup>

        <FormGroup>
          <Label for="">Route</Label>
          <RenderMultiSelect
            creatable
            placeholder={"Select or type to create new"}
            options={['3 ml SC', '2 ml IM']}
            onChange={(data) => {

            }}/>
        </FormGroup>

        <FormGroup>
          <Label for="">Available vaccines</Label>
          <RenderMultiSelect
            isMulti
            creatable
            placeholder={"Select or type to create new"}
            options={['BOVILIS FMDV GEL, FUTVAC', 'BOVILIS HS', 'Black Quarter Vaccines']}
            onChange={(data) => {

            }}/>
        </FormGroup>

        <FormGroup>
          <Label for="">Preferred month</Label>
          <RenderMultiSelect
            isMulti
            placeholder={"Select month"}
            options={['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']}
            onChange={(data) => {

            }}/>
        </FormGroup>

        <FormGroup>
          <Label for="">Remarks</Label>
          <Input type="text" placeholder="Enter remarks/comments"/>
        </FormGroup>

        <FormGroup>
          <Label for="exampleCheckbox">Status [all applicable cattle(s) will be notified timely]</Label>
          <div>
            <CustomInput
              onChange={(e) => {
                setVaccinationStatus(!vaccinationStatus);
              }}
              type="switch"
              id="vaccination_active_status"
              name="customSwitch"
              label={vaccinationStatus ? "Active" : "Inactive"}/>
          </div>
        </FormGroup>

        <FormGroup>
          <Label for="">Gender specific</Label>

          <div className="custom-control custom-radio">
            <input
              onChange={(e) => {
                if (e.target.checked) setGenderSpecific('Male');
              }}
              type="radio" className="custom-control-input" id="g_male" name="gender_radio" value="customEx"/>
            <Label className="custom-control-label" htmlFor="g_male">Male</Label>
          </div>
          <div className="custom-control custom-radio">
            <input
              onChange={(e) => {
                if (e.target.checked) setGenderSpecific('Female');
              }}
              type="radio" className="custom-control-input" id="g_female" name="gender_radio" value="customEx"/>
            <Label className="custom-control-label" htmlFor="g_female">Female</Label>
          </div>
          <div className="custom-control custom-radio">
            <input
              onChange={(e) => {
                if (e.target.checked) setGenderSpecific('Both');
              }}
              type="radio" className="custom-control-input" id="g_both" name="gender_radio"
              value="customEx"/>
            <Label className="custom-control-label" htmlFor="g_both">Both</Label>
          </div>
        </FormGroup>
        <Button
          onClick={() => {
            console.log(genderSpecific)
          }}
        >Submit</Button>
      </CardBody>
    </Card>
  </div>;
}