import React, {useState} from "react";
import {
  Card,
  CardBody,
  CardHeader, Col, Label, Row
} from 'reactstrap';
import SearchCow from "../../search_cow/SearchCow";
import {useHistory} from "react-router";
import EditDairy from "../cattle_profile/edit_profile/EditDairy";
import {getSpecificCowByName, logout, isValidToken} from "../../../services/HttpService";

export default function Dairy(props: any) {

  const history = useHistory();
  const [selectedCow, setSelectedCow] = useState('');

  if (!isValidToken(localStorage.jwt)) return logout();

  return <div className={"mt-2"}>
    <Row>
      <Col md={8} lg={6}>
        <Card className={""}>
          <CardHeader><strong>Add dairy</strong></CardHeader>
          <CardBody>
            <Label>Select cow then input milking info</Label>
            <SearchCow onCowSelected={(cowName) => {
              getSpecificCowByName(cowName).then(res => {
                if (res.data.type === "Dairy") setSelectedCow(cowName);
                else setSelectedCow("Selected cow is not Dairy type");
              }).catch(err => console.log(err));

            }}/>
            {!selectedCow.includes("Selected cow is") ?
              <EditDairy disabled={selectedCow === ""} name={selectedCow} onSave={() => setSelectedCow('')}/> :
              <h5 className={"mt-4 text-center"}> {selectedCow}</h5>}

          </CardBody>
        </Card>
      </Col>
    </Row>
  </div>
}