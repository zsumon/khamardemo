import React, {useEffect, useState} from "react";
import {
  Card,
  CardBody,
  CardHeader, Col, Label, Row
} from 'reactstrap';
import SearchCow from "../../search_cow/SearchCow";
import {useHistory} from "react-router";
import EditDisease from "../cattle_profile/edit_profile/EditDisease";
import {logout, isValidToken} from "../../../services/HttpService";

export default function Disease(props: any) {

  const history = useHistory();
  const [selectedCow, setSelectedCow] = useState('');

  if (!isValidToken(localStorage.jwt)) return logout();

  return <div className={"mt-2"}>
    <Row>
      <Col md="8" lg="6">
        <Card className={""}>
          <CardHeader><strong>Disease input</strong></CardHeader>
          <CardBody>
            <Label>Select cow then input disease info</Label>
            <SearchCow onCowSelected={(cowName) => {
              setSelectedCow(cowName);
            }}/>

            <EditDisease disabled={selectedCow === ""} name={selectedCow} onSave={() => setSelectedCow('')}/>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </div>
}