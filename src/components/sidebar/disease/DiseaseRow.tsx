import React, {useState} from "react";
import {
  Button,
  Col,
  Input,
  Row,
} from 'reactstrap';

import cattleAvatar from '../../../assets/img/cattle_img.jpg';
import RenderMultiSelect from "../Addons/RenderMultiSelect";

interface DiseaseRowProps {
  cattleName: string,

  onSaveClick?(diseaseName: string, comment: string, date: string, antibioticPush: boolean): void

}

export default function DiseaseRow(props: DiseaseRowProps) {

  const [diseaseName, setDiseaseName] = useState('');
  const [comment, setComment] = useState('');
  const [date, setDate] = useState('');
  const [antibiotic, setAntibiotic] = useState(false);

  return <div className={"d-flex align-items-center mb-2 pb-2 border-bottom"}>
    <div className="d-md-flex align-items-md-center text-center">
      <img style={{maxHeight: "4rem", maxWidth: "4rem"}} src={cattleAvatar} alt="" className={"text-center"}/>
      <p className="ml-1 mr-2">{props.cattleName}</p>
    </div>
    <Row className={"no-gutters"}>
      <Col md={"3"} className={"mr-1 mb-1"}>
        <RenderMultiSelect
          creatable
          options={['A', 'B']}
          placeholder={"Disease name"}
          onChange={({label, value}) => {
            setDiseaseName(value);
          }}/>
      </Col>
      <Col md={"3"} className={"mr-1 mb-1"}>
        <Input type={"text"} onChange={(e) => setComment(e.target.value)} placeholder={"Comment"}/>
      </Col>
      <Col md={"2"} className={"mr-1 mb-1"}>
        <Input type={"text"} onChange={(e) => setDate(e.target.value)} placeholder={"DD-MM-YY"}/>
      </Col>
      <Col md={"2"} className={"d-flex align-items-center ml-1 mb-2"}>
        <div className="custom-checkbox custom-control">
          <input type="checkbox"
                 onChange={(e) => {
                   e.target.checked ? setAntibiotic(true) : setAntibiotic(false);
                 }}
                 id={"cattle_antibiotic_" + props.cattleName}
                 className="custom-control-input"/>
          <label
            className="custom-control-label"
            htmlFor={"cattle_antibiotic_" + props.cattleName}>Antibiotic push</label>
        </div>
      </Col>
      <Col md={"1"} sm={"12"}>
        <Button onClick={() => {
          props.onSaveClick!(diseaseName, comment, date, antibiotic);
        }}>Save</Button>
      </Col>

    </Row>
  </div>
}