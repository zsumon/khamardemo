import React, {useState} from "react";
import {FormGroup, Input, Label, Button, Row, Col, CustomInput, Card, CardBody, CardHeader} from "reactstrap";
import {useHistory} from "react-router-dom";
import {saveDisease} from "../../../../services/HttpService";

interface EditDiseaseProps {
  name: string,
  disabled: boolean,

  onSave(): void,
}

export default function EditDisease(props: EditDiseaseProps) {
  const history = useHistory();

  const [disease, setDisease] = useState('BRDC');
  const [comment, setComment] = useState('');
  const [date, setDate] = useState(new Date().toJSON().slice(0, 10));
  const [antibiotic, setAntibiotic] = useState<boolean>(false);


  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader className={"d-none"}>
          <div className="d-flex align-items-baseline">
            {props.name && <i onClick={() => {
              history.goBack()
              // history.push('/cattle-profile?name=' + name)
            }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2 d-none"/>}
            <p className={"mb-0"} style={{fontSize: "1rem"}}>Disease input for {props.name}</p>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md={8} lg={6}>
              <FormGroup>
                <Label>Select disease</Label>
                <Input
                  disabled={props.disabled}
                  onChange={(e) => {
                    setDisease(e.target.value);
                  }} type={"select"} className={"mr-2"}>
                  <option>BRDC</option>
                </Input>
              </FormGroup>

              <FormGroup>
                <Label>Comment</Label>
                <Input
                  disabled={props.disabled}
                  onChange={(e) => {
                    setComment(e.target.value);
                  }} type={"text"} placeholder={"Enter comment"}/>
              </FormGroup>


              <FormGroup>
                <Label>Pick a date</Label>
                <Input
                  disabled={props.disabled}
                  onChange={(e) => {
                    setDate(e.target.value)
                  }} className={"mb-2"} style={{maxWidth: "11rem"}} type={"date"} value={date}/>
              </FormGroup>

              <FormGroup>
                <div>
                  <CustomInput
                    disabled={props.disabled}
                    onChange={(e) => {
                      if (e.target.checked) setAntibiotic(true)
                      else setAntibiotic(false)
                    }} type="checkbox" id="recent_antibiotic_push" label="Recent antibiotic push"/>
                </div>
              </FormGroup>

              <Button
                disabled={props.disabled}
                onClick={() => {
                  // save data to server
                  saveDisease(props.name, {
                    name: disease,
                    comment,
                    date,
                    antibiotic
                  }).then(res => {
                    console.log('saved disease', res);
                    props.onSave();
                  }).catch(err => console.log(err));
                }}
                className={"ml-2 mt-2"} color={"primary"}>Save</Button>
            </Col>
            <Col md={6}>
            </Col>
          </Row>
        </CardBody>
      </Card>

    </div>);
}