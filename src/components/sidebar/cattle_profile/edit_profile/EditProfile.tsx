import React, {useEffect, useState} from "react";
import {useHistory, useLocation} from "react-router-dom";
import {Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row} from "reactstrap";
import {useIntl} from "react-intl";
import {DATE_PLACEHOLDER} from "../../../../constants/ExtraConstants";
import Toast from "../../Addons/toast/Toast";
import infoIcon from "../../Addons/toast/check.svg";
import {getSpecificCowByName, updateCowById} from "../../../../services/HttpService";

interface EditProfileProps {
  name: string,
}

export default function EditProfile(props: EditProfileProps) {
  const location = useLocation();
  const history = useHistory();
  let name: string = (new URLSearchParams(location.search)).get("name")?.trim()!;
  if (name && name !== "" && name) {
  }
  const [editing, setEditing] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);

  const [editedName, setEditedName] = useState<string>(name);
  const [editedType, setEditedType] = useState<string>('');
  const [editedGeneticType, setEditedGeneticType] = useState<string>('');
  const [editedGeneticPercentage, setEditedGeneticPercentage] = useState<string>('');
  const [editedDateOfBirth, setEditedDateOfBirth] = useState<string>('');
  const [editedStatus, setEditedStatus] = useState('');
  const [updatedImage, setUpdatedImage] = useState('');

  const [id, setId] = useState<number>(0);
  const [cow, setCow] = useState<any>();

  useEffect(() => {
    getSpecificCowByName(name).then(res => {
      setEditedType(res.data.type);
      setEditedGeneticType(res.data.geneticType);
      setEditedGeneticPercentage(res.data.geneticPercentage);
      setEditedDateOfBirth(res.data.dateOfBirth);
      setId(res.data.id);
      setCow(res.data);
      setEditedStatus(res.data.status);
      setUpdatedImage(res.data.image);
      console.log('loaded cow profile', res.data);
    }).catch(err => console.log(err));
  }, [name]);

  const intl = useIntl();

  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader>
          {name ? <div className="d-flex align-items-baseline">
            <i onClick={() => {
              history.goBack()
            }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2"/>
            <p style={{fontSize: "1.5rem"}} className={"mb-0"}>Edit cattle profile</p>
          </div> : <p>Invalid name</p>}
        </CardHeader>

        {name && <CardBody>
          <Row>
            <Col md={6} lg={4}>
              <FormGroup>
                <Label htmlFor="cattle_name">{intl.formatMessage({id: 'text.cattle_name'})}</Label>
                <Input type="text"
                       value={editedName}
                       id="cattle_name"
                       placeholder={intl.formatMessage({id: 'enter.cattle_id_ear_tag'})}
                       onChange={(e) => {
                         setEditedName(e.target.value);
                       }}
                       required/>
              </FormGroup>
              <FormGroup>
                <Label for="cattle_type">{intl.formatMessage({id: 'cattle_type'})}</Label>
                <Input value={editedType} type={"select"} onChange={(e) => setEditedType(e.target.value)}>
                  <option>{intl.formatMessage({id: 'cattle_type.fattening'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_type.dairy'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_type.calf'})}</option>
                </Input>
              </FormGroup>

              <FormGroup>
                <Label for="cattle_genetic_type">{intl.formatMessage({id: 'cattle_genetic_type'})}</Label>
                <Input value={editedGeneticType} type={"select"}
                       onChange={(e) => setEditedGeneticType(e.target.value)}>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option1'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option2'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option3'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option4'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option5'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option6'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option7'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option8'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option9'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option10'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option11'})}</option>
                  <option>{intl.formatMessage({id: 'cattle_genetic_type.option12'})}</option>

                </Input>
              </FormGroup>


              <FormGroup>
                <Label for="cattle_genetic_percentage">{intl.formatMessage({id: 'cattle_genetic_percentage'})}</Label>
                <Input type="text"
                       id="cattle_genetic_percentage"
                       value={editedGeneticPercentage}
                       placeholder={intl.formatMessage({id: 'cattle_genetic_percentage'})}
                       onChange={(e) => setEditedGeneticPercentage(e.target.value)}
                       required/>
              </FormGroup>

              <FormGroup>
                <Label for="cattle_birth_date">{intl.formatMessage({id: 'cattle_birth_date'})}</Label>
                <Input type="text"
                       value={editedDateOfBirth}
                       onChange={(e) => {
                         setEditedDateOfBirth(e.target.value);
                       }}
                       placeholder={DATE_PLACEHOLDER}
                       id="cattle_birth_date"
                       required/>
              </FormGroup>

              <FormGroup>
                <Label for="cattle_status">Status</Label>
                <Input
                  onChange={(e) => {
                    console.log(e.target.value);
                    setEditedStatus(e.target.value)
                  }}
                  type="select" id="cattle_status"
                  value={editedStatus}>
                  <option value={"alive"}>Alive</option>
                  <option value={"sold"}>Sold</option>
                  <option value={"deceased"}>Deceased</option>
                </Input>
              </FormGroup>

              <FormGroup>
                <Label>Image</Label> <br/>
                {updatedImage && <img style={{maxWidth: "10rem", maxHeight: "10rem"}} src={updatedImage} alt=""/>}
                {/* <Input type={"file"} onChange={(e) => {
                  const reader = new FileReader();
                  // @ts-ignore
                  reader.onload = () => setUpdatedImage(reader.result.toString());
                  // @ts-ignore
                  reader.readAsDataURL(e.target.files[0]);
                }}/>*/}
              </FormGroup>


              <Button onClick={() => {
                setEditing(!editing);
                setSaved(true);
                setTimeout(() => {
                  setSaved(false);
                  // update info.../
                  updateCowById(id, {
                    name: editedName,
                    type: editedType,
                    geneticPercentage: editedGeneticPercentage,
                    geneticType: editedGeneticType,
                    dateOfBirth: editedDateOfBirth,
                    status: editedStatus,
                  }).then(res => {
                    history.goBack();
                    console.log('successfully updated cow profile', res.data);
                  }).catch(err => console.log('error updating cow profile: ' + err.toString()));

                }, 1000)
              }} color={"primary"}>Update profile info</Button>

              {saved &&
              <Toast
                toastList={[{
                  id: "1",
                  title: 'Success',
                  description: 'Cattle profile updated!',
                  backgroundColor: '#5cb85c',
                  icon: infoIcon
                }]}
                autoDelete={true}
                dismissTime={1500}
                position={"top-right"}/>}
            </Col>
          </Row>


        </CardBody>}
      </Card>
    </div>);
}