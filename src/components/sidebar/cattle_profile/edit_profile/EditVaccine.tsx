import React, {useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col, CustomInput, Card, CardBody, CardHeader} from "reactstrap";
import {useHistory, useLocation} from "react-router-dom";

interface EditVaccineProps {
  name: string,
}

export default function EditVaccine(props: EditVaccineProps) {
  const location = useLocation();
  const history = useHistory();
  let name: string = (new URLSearchParams(location.search)).get("name")?.trim()!;

  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader>
          <div className="d-flex align-items-baseline">
            {name && <i onClick={() => {
              history.goBack()
              // history.push('/cattle-profile?name=' + name)
            }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2"/>}
            <p className={"mb-0"} style={{fontSize: "1rem"}}>Vaccine input for {name}</p>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label>Select vaccine</Label>
                <Input style={{maxWidth: "10rem"}} type={"select"} className={"mr-2"}>
                  <option>BRDC</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Pick vaccination date</Label>
                <Input className={"mb-2"} style={{maxWidth: "11rem"}} type={"date"}
                       value={new Date().toJSON().slice(0, 10)}/>
              </FormGroup>
              <Button
                onClick={() => {
                  history.push("/cattle-profile?name=" + name);
                }}
                className={"ml-2 mt-2"} color={"primary"}>Save</Button>

            </Col>
            <Col md={6}>
            </Col>
          </Row>
        </CardBody>
      </Card>

    </div>);
}