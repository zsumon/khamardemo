import React, {useState} from "react";
import {FormGroup, Input, Button, Row, Col, CustomInput, Card, CardBody, CardHeader} from "reactstrap";
import {useHistory} from "react-router-dom";
import {saveReproduction} from "../../../../services/HttpService";

interface EditReproductionProps {
  name: string,
  disabled: boolean,

  onSave(): void,
}

const EditReproduction = (props: EditReproductionProps) => {

  const history = useHistory();
  //let name: string = (new URLSearchParams(location.search)).get("name")?.trim()!;

  const [date, setDate] = useState(new Date().toJSON().slice(0, 10));
  const [type, setType] = useState('Heat');

  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader className={"d-none"}>
          <div className="d-flex align-items-baseline">
            {props.name && <i onClick={() => {
              history.goBack()
            }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2 d-none"/>}
            <p className={"mb-0"} style={{fontSize: "1rem"}}>Reproduction input for {props.name}</p>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md={6}>
              <div className={"d-flex align-items-baseline"}>

                <FormGroup>
                  <div>
                    <CustomInput
                      disabled={props.disabled}
                      className={"mb-2"} type="radio" id="heat" name="repGroup" label="Heat"
                      htmlFor="heat"
                      checked={type === "Heat"}
                      onChange={(e) => {
                        if (e.target.checked) {
                          setType("Heat");
                        }
                      }}/>
                    <CustomInput
                      disabled={props.disabled}
                      className={"mb-2"} type="radio" id="semen" name="repGroup" label="Semen"
                      htmlFor="semen"
                      checked={type === "Semen"}
                      onChange={(e) => {
                        if (e.target.checked) {
                          setType("Semen");
                        }
                      }}/>
                    <CustomInput
                      disabled={props.disabled}
                      className={"mb-2"} type="radio" id="conceive" name="repGroup"
                      label="Conceive"
                      htmlFor="conceive"
                      checked={type === "Conceive"}
                      onChange={(e) => {
                        if (e.target.checked) {
                          setType("Conceive");
                        }
                      }}/>
                    <CustomInput
                      disabled={props.disabled}
                      className={"mb-2"} type="radio" id="delivery" name="repGroup"
                      label="Delivery"
                      htmlFor="delivery"
                      checked={type === "Delivery"}
                      onChange={(e) => {
                        if (e.target.checked) {
                          setType("Delivery");
                        }
                      }}/>
                    <div className="d-flex align-items-baseline">
                      <Input
                        disabled={props.disabled}
                        onChange={(e) => {
                          setDate(e.target.value);
                        }}
                        className={"mb-2"} style={{maxWidth: "11rem"}} type={"date"}
                        value={date}/>
                      <Button
                        disabled={props.disabled}
                        onClick={() => {
                          saveReproduction(props.name, {type, date}).then(res => {
                            console.log('saved reproduction,', res.data);
                            props.onSave();
                          }).catch(err => {
                            console.log("error saving reproduction data", err);
                          })
                        }}
                        className={"mt-2"} color={"primary"}>Save</Button>
                    </div>
                  </div>

                </FormGroup>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>

    </div>
  );
}

export default EditReproduction;