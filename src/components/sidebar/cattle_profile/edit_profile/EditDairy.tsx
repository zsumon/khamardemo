import React, {useState} from "react";
import {Input, Label, Button, Row, Col, Card, CardBody, CardHeader} from "reactstrap";
import {useHistory} from "react-router-dom";
import {saveDairy} from "../../../../services/HttpService";

interface EditDairyProps {
  name: string,
  disabled: boolean,

  onSave(): void,
}

const EditDairy = (props: EditDairyProps) => {
  const history = useHistory();

  const [date, setDate] = useState<string>(new Date().toJSON().slice(0, 10));
  const [milkAmount, setMilkAmount] = useState('');

  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader className={"d-none"}>
          <div className="d-flex align-items-baseline">
            {props.name && <i onClick={() => {
              history.goBack()
            }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2 d-none"/>}
            <p className={"mb-0"} style={{fontSize: "1rem"}}>Dairy input for {props.name}</p>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <div className="d-flex align-items-baseline flex-column">
                <Label>Milk amount (litre)</Label>
                <Input
                  disabled={props.disabled}
                  className={"mb-2 mr-2"} style={{maxWidth: "10rem"}} type={"text"}
                  placeholder={"Enter milk amount"}
                  value={milkAmount}
                  onChange={(e) => {
                    setMilkAmount(e.target.value);
                  }}/>
                <Label>Date</Label>
                <Input
                  disabled={props.disabled}
                  className={"mb-2"} style={{maxWidth: "11rem"}} type={"date"}
                  onChange={(e) => {
                    setDate(e.target.value);
                  }}
                  value={date}/>
                <Button
                  disabled={props.disabled}
                  onClick={() => {
                    if (milkAmount.trim() === "") {
                      alert("empty milk amount");
                      return;
                    }
                    saveDairy(props.name, {milkAmount: parseInt(milkAmount), date}).then(res => {
                      setMilkAmount('');
                      console.log('saved dairy: ', res);
                      props.onSave();
                    }).catch(err => {
                      console.log('error saving dairy', err);
                    });
                  }}
                  className={""} color={"primary"}>Save</Button>
              </div>

            </Col>
          </Row>
        </CardBody>
      </Card>

    </div>
  );
}

export default EditDairy;