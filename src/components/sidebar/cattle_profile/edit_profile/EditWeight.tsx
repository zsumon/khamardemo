import React, {useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col, CustomInput, Card, CardBody, CardHeader} from "reactstrap";
import {Line} from "react-chartjs-2";
import {useHistory, useLocation} from "react-router-dom";
import {saveWeight} from "../../../../services/HttpService";

interface EditWeightProps {
  name: string,
  disabled: boolean,

  onSave(): void,
}

export default function EditWeight(props: EditWeightProps) {

  const location = useLocation();
  const history = useHistory();
  // let name: string = (new URLSearchParams(location.search)).get("name")?.trim()!;
  const [date, setDate] = useState<string>(new Date().toJSON().slice(0, 10));
  const [weight, setWeight] = useState('');

  return (
    <div className={"mt-2"}>
      <Card>
        <CardHeader className={"d-none"}>
          <div className="d-flex align-items-baseline">
            {props.name && <i onClick={() => {
              history.goBack()
              // history.push('/cattle-profile?name=' + name)
            }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2 d-none"/>}

            <p className={"mb-0"} style={{fontSize: "1rem"}}>Weight input for {props.name}</p>
          </div>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <div className="d-flex align-items-baseline flex-column">

                <Label>Weight (kg)</Label>
                <Input
                  disabled={props.disabled}
                  value={weight}
                  onChange={(e) => {
                    setWeight(e.target.value);
                  }} className={"mb-2 mr-2"} style={{maxWidth: "10rem"}} type={"text"}
                  placeholder={"Enter weight"}/>

                <Label>Date</Label>
                <Input
                  disabled={props.disabled}
                  onChange={(e) => {
                    setDate(e.target.value);
                    console.log(date);
                  }}
                  className={"mb-2"} style={{maxWidth: "11rem"}} type={"date"}
                  value={date}/>

                <Button
                  disabled={props.disabled}
                  onClick={() => {
                    // save weight to the server
                    saveWeight(props.name, {
                      weight: parseInt(weight),
                      dateYYYYMMDD: date
                    }).then(res => {
                      props.onSave();
                    }).catch(err => console.log(err));
                  }}
                  className={""} color={"primary"}>Save</Button>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  );
}