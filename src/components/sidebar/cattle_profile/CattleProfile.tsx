import React, {ReactElement, useEffect, useState} from "react";
import {useHistory, useLocation} from "react-router-dom";
import './style.css';

import {
  Button,
  ButtonGroup,
  Card,
  CardBody,
  CardHeader, Col,
  Nav,
  NavItem,
  NavLink as NLink, Row,
  TabContent,
  TabPane
} from "reactstrap";
// resolve: { extensions: [".ts", ".tsx", ".js", ".jsx"] }, // added in => webpack config
import PPrimary from "./profile_details/PPrimary";
import PWeight from "./profile_details/weight-details/PWeight";
import PReproduction from "./profile_details/PReproduction";
import PMilkProduction from "./profile_details/PMilkProduction";
import PDisease from "./profile_details/disease-details/PDisease";
import PVaccine from "./profile_details/PVaccine";
import classNames from "classnames";
import {getAllCattle, getSpecificCowByName} from "../../../services/HttpService";
import SearchCow from "../../search_cow/SearchCow";

export enum ProfileTabs {PRIMARY, WEIGHT, REPRODUCTION, MILK_PRODUCTION, DISEASE, VACCINE, MEDICINE, OTHERS}

interface CattleProfileProps {

}

export default function CattleProfile(props: CattleProfileProps): ReactElement {
  const [activeTab, setActiveTab] = useState<ProfileTabs>(ProfileTabs.PRIMARY);
  const location = useLocation();
  const history = useHistory();
  let name: string = (new URLSearchParams(location.search)).get("name")?.trim()!;

  const [currentCow, setCurrentCow] = useState<any>({});
  const [cattleList, setCattleList] = useState<any>([]);

  useEffect(() => {
    getAllCattle().then(res => {
      setCattleList(res.data);
    }).catch(err => console.log(err));

    getSpecificCowByName(name).then(res => {
      setCurrentCow(res.data);
    }).catch(err => console.log('error loading specific cow: ', name, err));

  }, [name]);

  return (
    <Card className={"mt-2"}>
      <CardHeader>
        <div className="d-flex align-items-center">
          {name && <i onClick={() => {
            history.push('/cattle-profile')
          }} style={{fontSize: "1.3rem", cursor: "pointer"}} className="fa fa-arrow-left mr-2"/>}
          {currentCow.image &&
          <img className={"mr-2"} src={currentCow.image} style={{height: "2rem", width: "2rem"}} alt=""/>}
          <p className={"mb-0"} style={{fontSize: "1.5rem"}}>Cattle profile {name}</p>
        </div>
      </CardHeader>
      <CardBody>
        {/*specific cattle info when tag is valid*/}
        {name ? <div>
          <ButtonGroup className={"mb-4 d-none"}>
            <Button className="mr-1">12 litre milk</Button>
            <Button>Dry</Button>
          </ButtonGroup>

          <Nav tabs className={"nav-tabs__cp"}>
            <NavItem>
              <NLink
                className={classNames("nav-link__cp", {"active__cp active": activeTab === ProfileTabs.PRIMARY})}
                onClick={() => {
                  setActiveTab(ProfileTabs.PRIMARY)
                }}>Primary info</NLink>
            </NavItem>
            {currentCow.type === "Dairy" &&
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.MILK_PRODUCTION}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.MILK_PRODUCTION})}
                onClick={() => {
                  setActiveTab(ProfileTabs.MILK_PRODUCTION)
                }}>Milk production</NLink>
            </NavItem>}
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.WEIGHT}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.WEIGHT})}
                onClick={() => {
                  setActiveTab(ProfileTabs.WEIGHT)
                }}>Weight</NLink>
            </NavItem>
            {currentCow.type === "Dairy" &&
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.REPRODUCTION}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.REPRODUCTION})}
                onClick={() => {
                  setActiveTab(ProfileTabs.REPRODUCTION)
                }}>Reproduction</NLink>
            </NavItem>}
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.DISEASE}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.DISEASE})}
                onClick={() => {
                  setActiveTab(ProfileTabs.DISEASE)
                }}>Disease</NLink>
            </NavItem>
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.VACCINE}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.VACCINE})}
                onClick={() => {
                  setActiveTab(ProfileTabs.VACCINE)
                }}>Vaccine</NLink>
            </NavItem>
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.MEDICINE}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.MEDICINE})}
                onClick={() => {
                  setActiveTab(ProfileTabs.MEDICINE)
                }}>Medicine</NLink>
            </NavItem>
            <NavItem>
              <NLink
                active={activeTab === ProfileTabs.OTHERS}
                className={classNames("nav-link__cp", {"active__cp": activeTab === ProfileTabs.OTHERS})}
                onClick={() => {
                  setActiveTab(ProfileTabs.OTHERS)
                }}>Others</NLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={activeTab}>
            <TabPane tabId={ProfileTabs.PRIMARY}>
              <PPrimary name={name}/>
            </TabPane>
            <TabPane tabId={ProfileTabs.WEIGHT}>
              <PWeight name={name}/>
            </TabPane>
            <TabPane tabId={ProfileTabs.REPRODUCTION}>
              <PReproduction name={name}/>
            </TabPane>
            <TabPane tabId={ProfileTabs.MILK_PRODUCTION}>
              <PMilkProduction
                name={name}
                averageMilkProduction={cattleList.filter((it: any) => it.name.trim().toLowerCase() === name.trim().toLowerCase())[0]?.averageMilkAmount}/>
            </TabPane>
            <TabPane tabId={ProfileTabs.DISEASE}>
              <PDisease name={name}/>
            </TabPane>
            <TabPane tabId={ProfileTabs.VACCINE}>
              <PVaccine name={name}/>
            </TabPane>

          </TabContent>
        </div> : <div>
          <Row className={""}>
            <Col md={6} lg={4}>
              <p>Search: </p>
              <SearchCow onCowSelected={(cowName: string) => {
                history.push('/cattle-profile?name=' + cowName);
              }}/>
            </Col>
          </Row>

        </div>}
      </CardBody>
    </Card>);
}
