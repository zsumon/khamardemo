import React, {useEffect, useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col} from "reactstrap";
import {useHistory} from "react-router";
import {getDisease} from "../../../../../services/HttpService";

interface PDiseaseProps {
  name: string,
}

export default function PDisease(props: PDiseaseProps) {

  const history = useHistory();
  const [diseaseHistory, setDiseaseHistory] = useState<any[]>([]);

  useEffect(() => {
    getDisease(props.name).then(res => {
      setDiseaseHistory(res.data)
      console.log('dh', res.data);
    }).catch(err => console.log(err));
  }, []);

  return (
    <div>
      <Row>
        <Col md={6}>
          <Table responsive>
            <thead>
            <tr>
              <th>Date</th>
              <th>Disease</th>
            </tr>
            </thead>

            <tbody>

            {diseaseHistory.map((it, i) => (
              <tr key={i}>
                <td>{it.date.toString().substr(0, it.date.toString().indexOf('T')).split('-').reverse().join('-')}</td>
                <td>{it.name}</td>
              </tr>
            ))}
            <tr>
            </tr>
            </tbody>
          </Table>
        </Col>
        <Col md={6}>
        </Col>
      </Row>
      <Button
        onClick={() => {
          history.push('/edit-profile/disease?name=' + props.name);
        }}
        className={"ml-2 mt-2"} color={"primary d-none"}>Edit</Button>
    </div>
  );
}