import React, {useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col} from "reactstrap";
import {useHistory} from "react-router";

interface PVaccineProps {
  name: string,
}

export default function PVaccine(props: PVaccineProps) {

  const history = useHistory();
  const [vaccineHistory, setVaccineHistory] = useState([]);

  return (
    <div>
      <Row>
        <Col md={6}>
          <Table responsive>
            <thead>
            <tr>
              <th>Date</th>
              <th>Vaccine</th>
            </tr>
            </thead>

            <tbody>

            {vaccineHistory.map((it: any, i) => (
              // todo insha'Allah
              <tr key={i}>
                <td>{it.date}</td>
                <td>{it.name}</td>
              </tr>
            ))}

            <tr>
              <td>10-02-17</td>
              <td>ABC</td>
            </tr>
            <tr>
              <td>10-02-18</td>
              <td>XYZ</td>
            </tr>
            <tr>
              <td>10-02-20</td>
              <td>others</td>
            </tr>
            </tbody>
          </Table>
        </Col>
        <Col md={6}>
        </Col>
      </Row>
      <Button onClick={() => {
        history.push('/edit-profile/vaccine?name=' + props.name);
      }} color={"primary"} className={"d-none"}>Edit</Button>
    </div>
  );
}