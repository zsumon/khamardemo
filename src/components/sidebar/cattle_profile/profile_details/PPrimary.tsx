import React, {useEffect, useState} from "react";
import {FormGroup, Input, Label, Button, Col, Row} from "reactstrap";
import {useIntl} from "react-intl";
import Toast from '../../Addons/toast/Toast.js';
import infoIcon from '../../Addons/toast/check.svg';
import {useHistory} from "react-router";
import {getSpecificCowByName} from "../../../../services/HttpService";

interface ProfilePrimaryProps {
  name: string,
}

export default function PPrimary(props: ProfilePrimaryProps) {
  const formGroupClassName = 'd-flex justify-content-between align-items-baseline';
  const history = useHistory();
  const intl = useIntl();

  const [cattle, setCattle] = useState<any>();

  useEffect(() => {
    if (props.name && props.name !== "" && props.name) {
      getSpecificCowByName(props.name).then(res => {
        setCattle(res.data);
      }).catch(err => console.log(err));
    }
  }, []);

  return (<>
      {cattle && <div>
        <div className="row">
          <div className="col">
            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_name"}>Name</Label>
              </div>
              <div className="col-sm-6 col-md-2">
                <FormGroup className={formGroupClassName}>
                  <p>{cattle.name}</p>
                </FormGroup>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_type"}>Type</Label>
              </div>
              <div className="col-sm-6 col-md-2">
                <FormGroup className={formGroupClassName}>
                  <p>{cattle.type}</p>
                </FormGroup>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_gtype"}>Genetic type</Label>
              </div>
              <div className="col-2">
                <FormGroup className={formGroupClassName}>
                  <p>{cattle.geneticType}</p>
                </FormGroup>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_gp"}>Genetic percentage</Label>
              </div>
              <div className="col-sm-6 col-md-2">
                <FormGroup className={formGroupClassName}>
                  <p>{cattle.geneticPercentage}</p>
                </FormGroup>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_dob"}>Date of birth</Label>
              </div>
              <div className="col-sm-6 col-md-2">
                <FormGroup className={formGroupClassName}>
                  <p>{cattle.dateOfBirth}</p>
                </FormGroup>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_status"}>Status</Label>
              </div>
              <div className="col-sm-6 col-md-2">
                <FormGroup className={formGroupClassName}>
                  <p>{cattle.status}</p>
                  {/*<p>Alive</p>*/}
                </FormGroup>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-6 col-md-2">
                <Label for={"cattle_status"}>Image</Label>
              </div>
              <div className="col-sm-6 col-md-2">
                <FormGroup className={formGroupClassName}>
                  <img src={cattle.image} alt="" style={{maxHeight: "10rem", maxWidth: "10rem"}}/>
                </FormGroup>
              </div>
            </div>

          </div>
        </div>

        <Row>
          <Col md={6}>
            <Button onClick={() => {
              history.push('/edit-profile/basic?name=' + props.name);
            }} color={"primary"}>Edit</Button>
          </Col>
        </Row>
      </div>
      }</>
  );
}