import React, {useEffect, useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col} from "reactstrap";
import {Line} from "react-chartjs-2";
import {getLatestDairy, getMilkHistory, getSpecificCowByName, getWeight} from "../../../../services/HttpService";
import {useHistory} from "react-router";

interface ProfileMilkProductionProps {
  name: string,
  averageMilkProduction: number,
}

export default function PMilkProduction(props: ProfileMilkProductionProps) {

  const history = useHistory();

  const [milkHistory, setMilkHistory] = useState<any>([]);
  const data = {
    labels: milkHistory.map((it: any) => it.date.substr(0, it.date.indexOf('T'))).reverse(),
    datasets: [
      {
        label: 'Milk',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: milkHistory.map((it: any) => it.milkAmount).reverse()
      }
    ]
  };

  function loadLatestDairy() {
    getSpecificCowByName(props.name).then(res => {
      getLatestDairy(Number(res.data.id)).then(res1 => setMilkHistory(res1.data)).catch(err => console.log(err));
    }).catch(err => console.log(err));
  }

  useEffect(() => {
    loadLatestDairy();
  }, [props.name]);

  return (
    <div>
      <Row>
        <Col md={6}>
          <Table responsive>
            <thead>
            <tr>
              <th>Date</th>
              <th>Weight</th>
            </tr>
            </thead>
            <tbody>
            {milkHistory.map((it: any, i: number) => (
              <tr key={i}>
                <td>{it.date.substr(0, it.date.search('T'))}</td>
                <td>{it.milkAmount}</td>
              </tr>))}
            </tbody>
          </Table>
        </Col>
        <Col md={6}>
          <Line data={data}/>
        </Col>
      </Row>

      <Button
        onClick={() => {
          history.push('/edit-profile/dairy?name=' + props.name);
        }}
        className={"ml-2 mt-2 d-none"} color={"primary"}>Edit</Button>
    </div>
  );
}