import React, {useState} from "react";
import {saveWeight, updateCowById, updateWeight} from "../../../../../services/HttpService";


interface EditableWeightRowProps {
  cow: any,

  onSave(): void,
}

const EditableWeightRow = (props: EditableWeightRowProps) => {

  const [weight, setWeight] = useState<number>(props.cow.weight);
  const [date, setDate] = useState<string>(props.cow.weightTakingDate)

  const [isEditing, setIsEditing] = useState<boolean>();

  const editRet = <tr>
    <td>
      <input type="text" style={{maxWidth: "3rem"}} value={weight}
             onChange={(e) => {
               const wt = parseInt(e.target.value);
               if (wt > 0 && wt <= 1000) {
                 setWeight(parseInt(e.target.value))
               } else {
                 setWeight(0);
               }
             }}/>
    </td>
    <td>
      <input type="date" style={{maxWidth: "10rem"}} value={date} onChange={(e) => setDate(e.target.value)}/>
    </td>
    <td className={""}>
      <button onClick={() => {
        if (isEditing) {
          //save it to database...
          updateWeight({
            id: props.cow.id,
            weight: weight,
            weightTakingDate: date
          }).then(res => {
            setIsEditing(false);
            props.onSave();
            console.log('updated weight: ', res)
          }).catch(err => {
            console.log(err)
          });
        }
        setIsEditing(prevState => !prevState)
      }}>{isEditing ? "Save" : "Update"}</button>
    </td>
  </tr>;

  const nonEditRet = <tr>
    <td>{props.cow.weight}</td>
    <td>{props.cow.weightTakingDate}</td>
    <td className={""}>
      <button onClick={() => {
        if (isEditing) props.onSave();
        setIsEditing(prevState => !prevState)
      }}>{isEditing ? "Save" : "Update"}</button>
    </td>
  </tr>;

  if (isEditing) return editRet;
  return nonEditRet;
}
export default EditableWeightRow;