import React, {useEffect, useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col} from "reactstrap";
import {Line} from "react-chartjs-2";
import {getLatestDairy, getLatestWeight, getSpecificCowByName, getWeight} from "../../../../../services/HttpService";
import {useHistory} from "react-router";
import EditableWeightRow from "./EditableWeightRow";
import {dates} from "../../../../../constants/Utils";

interface ProfileWeightProps {
  name: string,
}

export default function PWeight(props: ProfileWeightProps) {
  const [weightHistory, setWeightHistory] = useState<any>([]);
  const history = useHistory();

  const data = {
    labels: weightHistory.map((it: any) => it.weightTakingDate).reverse(),
    datasets: [
      {
        label: 'Weight',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: weightHistory.map((it: any) => it.weight).reverse()
      }
    ]
  };

  useEffect(() => {
    loadLatestWeight();
  }, [props.name]);

  function loadLatestWeight() {
    getSpecificCowByName(props.name).then(res => {
      getLatestWeight(Number(res.data.id)).then(res1 => setWeightHistory(res1.data)).catch(err => console.log(err));
    }).catch(err => console.log(err));
  }

  const [isEditing, setIsEditing] = useState<boolean>(false);

  return (
    <div>
      <Row>
        <Col md={6}>
          <Table responsive>
            <thead>
            <tr>
              <th>Weight</th>
              <th>Date</th>
              <th>Update</th>
            </tr>
            </thead>

            <tbody>

            {weightHistory.map((it: any, i: number) => (
              <EditableWeightRow key={i} cow={it} onSave={() => {
                loadLatestWeight();
              }}/>
            ))}
            </tbody>
          </Table>
        </Col>
        <Col md={6}>
          <Line data={data}/>
        </Col>
      </Row>
      <Button
        onClick={() => {
          history.push('/edit-profile/weight?name=' + props.name);
        }}
        className={"ml-2 mt-2 d-none"} color={"primary"}>Edit</Button>
    </div>
  );
}