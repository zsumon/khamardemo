import React, {useEffect, useState} from "react";
import {FormGroup, Input, Label, Button, Table, Row, Col, CustomInput} from "reactstrap";
import {Line} from "react-chartjs-2";
import {useHistory, useLocation} from "react-router-dom";
import {getReproduction} from "../../../../services/HttpService";
import {collapseDuration} from "react-select/src/animated/transitions";

interface ProfileReproductionProps {
  name: string,
}

export default function PReproduction(props: ProfileReproductionProps) {

  const location = useLocation();
  const history = useHistory();
  let name: string = (new URLSearchParams(location.search)).get("name")?.trim()!;

  const [reproductionList, setReproductionList] = useState([]);

  useEffect(() => {
    getReproduction(name).then(res => {
      setReproductionList(res.data);
      console.log('loaded reproduction list: ', res.data);
    }).catch(err => {
      console.log('error loading reproduction history: ', err);
    });
  }, [name]);

  return (
    <div>
      <Row>
        <Col md={6}>

          <Table responsive className={"mt-4"}>
            <thead>
            <tr>
              <th>Date</th>
              <th>Status</th>
            </tr>
            </thead>

            <tbody>

            {reproductionList.map((it: any, i) => (<tr key={i}>
              <td>{it.date}</td>
              <td>{it.type}</td>
            </tr>))}

            </tbody>

          </Table>
        </Col>
        <Col md={6}>
        </Col>
      </Row>
      <Button
        onClick={() => {
          //@ts-ignore
          history.push("/edit-profile/reproduction?name=" + name);
        }}
        className={"ml-2 mt-2 d-none"} color={"primary"}>Edit</Button>
    </div>);
}