import * as React from "react";
import {Route, Redirect, withRouter} from "react-router-dom";

class PrivateRoute extends React.Component {
  render() {
    if (localStorage.jwt == null) {
      return (<Redirect to={{
        pathname: "/login",
        state: {from: this.props.location}
      }}/>)
    }
    return (this.props.children)

  }
}

export default PrivateRoute;