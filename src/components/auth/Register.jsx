import React, {useState} from 'react';
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row, FormGroup, Label
} from 'reactstrap';

import {Link, Redirect} from "react-router-dom";

import spinner from '../../assets/img/Spinner-1s-200px.gif';
import {BASE_URL, isUserNameAvailable} from "../../services/HttpService";

function Register(props) {

  const [isRegistered, setIsRegistered] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isAvailable, setIsAvailable] = useState(false);
  const [userName, setUserName] = useState('')
  const [fullName, setFullName] = useState('')


  if (isRegistered) {
    return (
      <Redirect to={{
        pathname: "/login",
        state: {from: props.location}
      }}/>
    );
  }

  function handleRegistration(e) {
    e.preventDefault();
    const pass = e.target["password"].value;
    const axios = require("axios");
    const postData = {userName, fullName, password: pass};

    if (userName.trim() === "") {
      alert("Invalid username");
      return;
    }
    if (pass.trim() === "") {
      alert("Invalid password");
      return;
    }

    // console.log(userName, pass);

    axios.post(BASE_URL + "/register", postData)
      .then(res => {
        if (res.data === "User already exists") {
          alert("user already exists!");
        } else {
          setIsRegistered(true);
        }
      })
      .catch(err => console.log(err));
  }

  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="9" lg="7" xl="6">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Form onSubmit={handleRegistration}>
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" name="name" placeholder="Full Name"
                           onChange={(e) => setFullName(e.target.value)}
                           autoComplete="name"/>
                  </InputGroup>
                  <InputGroup className="mb-3 d-none">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" name="designation" placeholder="Designation"
                           autoComplete="designation"/>
                  </InputGroup>

                  <div className="d-flex align-items-center">
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        onChange={(e) => {
                          const uName = e.target.value;
                          if (uName.trim() === "") return;
                          setUserName(uName.trim());
                          setIsLoading(true);
                          isUserNameAvailable(uName).then(res => {
                            setTimeout(() => {
                              if (res.data) {
                                setIsLoading(false);
                                setIsAvailable(true);
                              } else {
                                setIsLoading(false);
                                setIsAvailable(false);
                              }
                            }, 0);
                          }).catch(err => {
                            console.log(err);
                            setIsLoading(false);
                          });
                        }}
                        type="text"
                        name="username"
                        placeholder="Username"
                        autoComplete="username"/>
                    </InputGroup>
                    {isLoading &&
                    <img src={spinner} style={{width: "3rem", height: "3rem"}} className={"mb-3"} alt=""/>}
                    {isAvailable && (userName !== "") &&
                    <i style={{fontSize: "2rem"}} className={"icon-check mb-3 ml-1"}/>}
                    {!isAvailable && (userName !== "") && !isLoading &&
                    <i style={{fontSize: "2rem"}} className={"icon-ban mb-3 ml-1"}/>}

                  </div>

                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"/>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="password" name="password" placeholder="Temporary Password"
                           autoComplete="new-password"/>
                  </InputGroup>


                  <FormGroup className={"d-none"}>
                    <Label>Role</Label>
                    <div className="custom-control custom-radio mb-1">
                      <input type="radio" className="custom-control-input"
                             id="role-farm-owner"
                             name="radio-stacked"/>
                      <label className="custom-control-label"
                             htmlFor="role-farm-owner">Farm owner</label>
                    </div>
                    <div className="custom-control custom-radio mb-1">
                      <input type="radio" className="custom-control-input"
                             id="role-admin"
                             name="radio-stacked"/>
                      <label className="custom-control-label"
                             htmlFor="role-admin">Admin</label>
                    </div>
                    <div className="custom-control custom-radio mb-1">
                      <input type="radio" className="custom-control-input"
                             id="role-manager"
                             name="radio-stacked"/>
                      <label className="custom-control-label"
                             htmlFor="role-manager">Manager</label>
                    </div>
                    <div className="custom-control custom-radio mb-1">
                      <input type="radio" className="custom-control-input"
                             id="role-worker"
                             name="radio-stacked"/>
                      <label className="custom-control-label"
                             htmlFor="role-worker">Worker</label>
                    </div>
                  </FormGroup>

                  <Button color="success" block>Create Account</Button>
                  <p className="mt-2 mb-1">Already have an account?</p>
                  <Link to="/login" className="d-flex justify-content-center ">
                    <Button style={{width: "100%"}} color="primary" className="mt-3" active>Login</Button>
                  </Link>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>);
}

export default Register;
