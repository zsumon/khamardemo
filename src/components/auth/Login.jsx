import React, {useState} from 'react';
import {Link, Redirect} from 'react-router-dom';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';

import axios from "axios";
import {BASE_URL, isValidToken} from "../../services/HttpService";
import spinner from '../../assets/img/Spinner-1s-200px.gif';

const Login = (props) => {

  const [isLoading, setIsLoading] = useState(false);
  const [token, setToken] = useState('');

  const handleLogin = (e) => {
    e.preventDefault();
    const userName = e.target["username"].value;
    const password = e.target["password"].value;

    if (userName === "") {
      alert("empty username");
      return;
    }
    if (password === "") {
      alert("empty password");
      return;
    }
    setIsLoading(true);
    // call to server to handle login..
    axios.post(BASE_URL + "/auth", {
      userName, password
    }).then(res => {
      setIsLoading(false);
      console.log(res.data);
      if (res.data === "Bad credential") {
        alert("Bad credential");
      } else {
        localStorage.jwt = res.data.jwt;
        setToken(res.data.jwt);
      }
    }).catch(err => {
      setIsLoading(false);
      console.log(err);
    });
  };

  if (isValidToken(token)) {
    return <Redirect to={{
      pathname: "/",
      state: {from: props.location}
    }}/>
  }

  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="8">
            <CardGroup>
              <Card className="p-4">
                <CardBody>
                  <Form onSubmit={(e) => handleLogin(e)}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="username" placeholder="Username"
                             autoComplete="username"/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" name="password" placeholder="Password"
                             autoComplete="current-password"/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <div className="d-flex">
                          <Button
                            color="primary"
                            className="px-4">Login</Button>
                          {isLoading && <img src={spinner} style={{width: "100%", maxHeight: "2rem"}} alt=""/>}
                        </div>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                    <Link to="/register" className="d-md-flex justify-content-center d-lg-none ">
                      <Button color="primary" className="mt-3" active tabIndex={-1}>Register
                        Now!</Button>
                    </Link>
                  </Form>
                </CardBody>
              </Card>
              <Card className="text-white bg-primary py-5 d-md-down-none" style={{width: '44%'}}>
                <CardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>Sign up for a new account</p>
                    <Link to="/register">
                      <Button color="primary" className="mt-3" active tabIndex={-1}>Register
                        Now!</Button>
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
      </Container>
    </div>
  );

}

export default Login;
