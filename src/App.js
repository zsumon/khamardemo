import React, {useState} from 'react';

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import DefaultLayout from "./components/container/DefaultLayout";
import {IntlProvider} from "react-intl"

import bangla from './constants/translation/bangla.json'
import english from './constants/translation/english.json'
import PrivateRoute from "./components/auth/PrivateRoute";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";

const App = (props) => {
  const [lang, setLang] = useState('en');

  const message = {
    'en': english,
    'bn': bangla
  };

  return (
    <IntlProvider locale={lang} messages={message[lang]}>
      <Router>
        <Switch>
          <Route exact path="/login"><Login/></Route>
          <Route exact path="/register"><Register/></Route>
          <PrivateRoute>
            <DefaultLayout onLanguageChange={() => {
              if (lang === "en") setLang('bn');
              else setLang('en');
            }}/>
          </PrivateRoute>
        </Switch>
      </Router>
    </IntlProvider>
  );
}

export default App;