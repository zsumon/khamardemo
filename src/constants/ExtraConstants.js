
export const DATE_PLACEHOLDER = "DD-MM-YYYY";
export const enCow = "Cow";
export const bnCow = "গাভী";
export const CURRENT_DATE = new Date().toJSON().slice(0, 10).split('-').reverse().toString().replace(/,/g, '-');