/*Types & drop-downs should be concerned later*/
import AddCattle from "../components/sidebar/cattle_record/AddCattle";
import AllCattle from "../components/sidebar/cattle_record/AllCattle";

import Dairy from "../components/sidebar/dairy/Dairy";
import Vaccination from "../components/sidebar/vaccination/Vaccination";
import Disease from "../components/sidebar/disease/Disease";
import DailyFeed from "../components/sidebar/DailyFeed";
import Ration from "../components/sidebar/ration/Ration";
import Conceive from "../components/sidebar/reproduction/Reproduction";
import Weight from "../components/sidebar/weight/Weight";
import AddFeedIngredient from "../components/sidebar/ration/AddFeedIngredient";
import CattleProfile from "../components/sidebar/cattle_profile/CattleProfile";
import EditProfile from "../components/sidebar/cattle_profile/edit_profile/EditProfile";
import PWeight from "../components/sidebar/cattle_profile/profile_details/weight-details/PWeight";
import EditWeight from "../components/sidebar/cattle_profile/edit_profile/EditWeight";
import EditReproduction from "../components/sidebar/cattle_profile/edit_profile/EditReproduction";
import EditDairy from "../components/sidebar/cattle_profile/edit_profile/EditDairy";
import EditDisease from "../components/sidebar/cattle_profile/edit_profile/EditDisease";
import EditVaccine from "../components/sidebar/cattle_profile/edit_profile/EditVaccine";
import Reproduction from "../components/sidebar/reproduction/Reproduction";

export const navigation = {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'All cow',
      url: '/all-cow',
      icon: 'icon-list',
    },
    {
      name: 'View profile',
      url: '/cattle-profile',
      icon: 'icon-info',
    },
    {
      name: 'Add cow',
      url: '/register-cow',
      icon: 'icon-magnifier-add',
    },
    {
      name: 'Add dairy',
      url: '/dairy',
      icon: 'icon-bag',
    },
    {
      name: 'Add weight',
      url: '/weight',
      icon: 'icon-info',
    },
    {
      name: 'Add disease',
      url: '/disease',
      icon: 'icon-info',
    },
    {
      name: 'Add reproduction',
      url: '/reproduction',
      icon: 'icon-info',
    },
  ]
};

export const routes = [
  {path: '/', name: 'Home'},
  {path: '/register-cow', name: 'Add cattle', component: AddCattle},
  {path: '/all-cow', name: 'All cattle', component: AllCattle},
  {path: '/vaccination', name: 'Vaccination', component: Vaccination},
  {path: '/daily-feed', name: 'Daily feed', component: DailyFeed},
  {path: '/add-ration', name: 'Add ration', component: Ration},
  {path: '/dairy', name: 'Calf', component: Dairy},
  {path: '/disease', name: 'Disease', component: Disease},
  {path: '/weight', name: 'Weight', component: Weight},
  {path: '/reproduction', name: 'Reproduction', component: Reproduction},
  {path: '/feed-ingredient', name: 'Feed ingredient', component: AddFeedIngredient},

  {path: '/cattle-profile', name: 'Cattle profile', component: CattleProfile},

  {path: '/edit-profile/basic', name: 'Edit basic info', component: EditProfile},
  {path: '/edit-profile/weight', name: 'Edit weight', component: EditWeight},
  {path: '/edit-profile/reproduction', name: 'Edit reproduction', component: EditReproduction},
  {path: '/edit-profile/dairy', name: 'Edit dairy', component: EditDairy},
  {path: '/edit-profile/disease', name: 'Edit disease', component: EditDisease},
  {path: '/edit-profile/vaccine', name: 'Edit vaccine', component: EditVaccine},

];
