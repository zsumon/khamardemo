import firebase from 'firebase';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD42i_DoowLoMaOjncIVpFuElZ6moq6txA",
  authDomain: "khamar360.firebaseapp.com",
  databaseURL: "https://khamar360.firebaseio.com",
  projectId: "khamar360",
  storageBucket: "khamar360.appspot.com",
  messagingSenderId: "600608567741",
  appId: "1:600608567741:web:95654c7b9a27726d62a2d4"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const storage = firebase.storage();

export {
  storage, firebase as default
}