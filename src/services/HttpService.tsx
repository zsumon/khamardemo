import axios from 'axios';
import {Redirect} from "react-router-dom";
import React from "react";

const formatDateForServer = (date: string): string => date.trim().split('-').reverse().join('-').toString();

const get = (url: string): Promise<any> => axios.get(url, {headers: {"Authorization": "Bearer " + localStorage.jwt}});


const post = (url: string, data: any): Promise<any> => {
  const options: any = {
    method: 'POST',
    url: url,
    headers: {
      'Authorization': 'Bearer ' + localStorage.jwt,
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: data
  };
  return axios(options);
}
// export const BASE_URL = "https://shrouded-bastion-33668.herokuapp.com";
export const BASE_URL = "http://localhost:8080";

export const getAllCattle = () => get(BASE_URL + "/all-cattle");

export function saveCow(cowData: any) {
  return post(BASE_URL + "/add-cattle", cowData);
}

export const saveWeight = (cattleName: string, weightData: { weight: number, dateYYYYMMDD: string }) => {
  return post(BASE_URL + "/add-weight/" + cattleName, {
    weight: weightData.weight,
    weightTakingDate: weightData.dateYYYYMMDD
  });
}

export const saveDairy = (cattleName: string, dairyData: { milkAmount: number, date: string }) => {
  return post(BASE_URL + "/add-dairy/" + cattleName, {
    milkAmount: dairyData.milkAmount,
    date: dairyData.date
  });
}

export function saveDisease(cowName: string, diseaseData: { name: string, comment: string, date: string, antibiotic: boolean }) {
  return post(BASE_URL + "/add-disease/" + cowName, {
    name: diseaseData.name,
    comment: diseaseData.comment,
    date: diseaseData.date,
    recentAntiBioticPush: diseaseData.antibiotic
  });
}

export const saveReproduction = (cowName: string, reproductionData: { type: string, date: string }) => {
  return post(BASE_URL + "/add-reproduction/" + cowName, {
    type: reproductionData.type,
    date: reproductionData.date
  });
}

export const getSpecificCowByName = (cowName: string) => {
  return get(BASE_URL + "/cattle/" + cowName);
}

export const getWeight = (cowName: string) => {
  return get(BASE_URL + "/get-weight/" + cowName);
}
export const getDisease = (cowName: string) => {
  return get(BASE_URL + "/get-disease/" + cowName);
}

export function getMilkHistory(cowName: string) {
  return get(BASE_URL + "/get-dairy/" + cowName);
}

export function searchCow(cowName: string) {
  return get(BASE_URL + "/search-cow/" + cowName.toLowerCase())
}

export function getReproduction(cowName: string) {
  return get(BASE_URL + "/get-reproduction/" + cowName);
}

export function getVaccine(cowName: string) {
  return get(BASE_URL + "/get-vaccine/" + cowName);
}

export function updateCowById(id: Number, cow: {
  name: string, type: string, geneticType: string,
  geneticPercentage: string, dateOfBirth: string,
  status: string
}) {
  return post(BASE_URL + "/update-cow/" + id, cow);
}

export function isUserNameAvailable(userName: string) {
  return axios.get(BASE_URL + "/isAvailable/" + userName);
}

export function upload(image: any) {
  return post(BASE_URL + "/upload", image);
}

export function updateWeight(data: { id: number, weight: number, weightTakingDate: string, }) {
  return post(BASE_URL + "/update-weight/", data);
}

export function isValidToken(token: string) {
  if (token == null || token.length === 0) return false;

  const jwtDecode = require('jwt-decode');
  const tok = jwtDecode(token);
  const expDate = tok.exp * 1000;
  const current = new Date().getTime();
  const diff = expDate - current;
  if (diff > 0) {
    // console.log('token is valid');
    //console.log('token will expired at: ' + expDate, "current is: " + current + " diff: " + diff);
    return true;
  }
  // console.log('token expired at: ' + expDate, "current is: " + current + " diff: " + diff);
  console.log('token is invalid');
  return false;
}

export function clearJwt() {
  localStorage.removeItem("jwt");
}

export function logout() {
  clearJwt();
  return (
    <Redirect to={{
      pathname: "/login",
    }}/>
  );
}

export function paginatedGetCow(page: number, limit: number) {
  return get(BASE_URL + `/get-cow?page=${page}&limit=${limit}`);
}

export function getLatestWeight(cowId: number) {
  return get(BASE_URL + "/get-latest-weight/" + cowId);
}

export function getLatestDairy(cowId: number) {
  return get(BASE_URL + "/get-latest-dairy/" + cowId);
}